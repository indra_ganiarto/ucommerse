## Larulin Boilerplate

[![Build Status](https://travis-ci.org/laravel/framework.svg)](https://travis-ci.org/laravel/framework)
[![Total Downloads](https://poser.pugx.org/laravel/framework/downloads.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Stable Version](https://poser.pugx.org/laravel/framework/v/stable.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Unstable Version](https://poser.pugx.org/laravel/framework/v/unstable.svg)](https://packagist.org/packages/laravel/framework)
[![License](https://poser.pugx.org/laravel/framework/license.svg)](https://packagist.org/packages/laravel/framework)

Larulin aja namanya.

Belum semua fitur yang ada di repo ini baru core aja antara lain:

* Auth User
* User Roles
* Roles Permission
* Dynamic Menu

Package yang available:
(cek di composer.json)
 

Package yang digunakan:
(cek di app.php)

## Documentation

Documentation for the entire framework can be found in the code.

### Contributing To Larulin

**All issues and pull requests should be filed on the [larulin](https://bitbucket.org/ulincommerce/larulin) repository.**

### License

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)
