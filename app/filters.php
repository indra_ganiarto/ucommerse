<?php

/*
|--------------------------------------------------------------------------
| Application & Route Filters
|--------------------------------------------------------------------------
|
| Below you will find the "before" and "after" events for the application
| which may be used to do any work before or after a request into your
| application. Here you may also register your custom route filters.
|
*/

App::before(function($request)
{
	// Share variables for all Controller
	App::singleton('admin_url', function(){
		$admin_url = Config::get('larulin.system.admin_url');
		return $admin_url;
	});

	App::singleton('cache_user', function(){
		
		$expiresAt = Carbon::now()->addMinutes(1);

		if(Cache::has('cur_user')){			
			$cur_user = Cache::get('cur_user');
		}else{
			Cache::put('cur_user', Auth::user(), $expiresAt);
			$cur_user = Cache::get('cur_user');
		}

		return $cur_user;

	});
	App::singleton('cache_sys_menu', function(){

		$expiresAt = Carbon::now()->addMinutes(1);

		if(Cache::has('sys_menu')){
			$sys_menu = Cache::get('sys_menu');
		}else{
			Cache::put('sys_menu',
				MenuItem::orderBy('id','ASC')->get()->toHierarchy(),
				$expiresAt
			);
			$sys_menu = Cache::get('sys_menu');
		}

		return $sys_menu;

	});
		// Share variables for all views
	View::composer(['backend.*','frontend.*','403','404','503'], function($view) {

		$cur_user = app('cache_user');
		$sys_menu = app('cache_sys_menu');
		$view->with('com_profile', Config::get('larulin.profile'));
		$view->with('admin_url', Config::get('larulin.system.admin_url')); 
		$view->with('sys_menu', $sys_menu);


		if(Auth::check()){

		    $cur_user_roles = $cur_user->roles;
			$cur_user_perms = $cur_user_roles[0]->perms;
			$view->with('cur_user', $cur_user);
			$view->with('cur_user_roles', $cur_user_roles);
			$view->with('cur_user_perms', $cur_user_perms);

		}else{

			$view->with('cur_user', NULL);
		
		}

	});

});


App::after(function($request, $response)
{
	//
});

/*
|--------------------------------------------------------------------------
| Authentication Filters
|--------------------------------------------------------------------------
|
| The following filters are used to verify that the user of the current
| session is logged into this application. The "basic" filter easily
| integrates HTTP Basic authentication for quick, simple checking.
|
*/

Route::filter('auth', function()
{
	if (Auth::guest())
	{
		if (Request::ajax())
		{
			return Response::make('Unauthorized', 401);
		}
		else
		{
			return Redirect::guest('login');
		}
	}
});


Route::filter('auth.basic', function()
{
	return Auth::basic();
});

/*
|--------------------------------------------------------------------------
| Guest Filter
|--------------------------------------------------------------------------
|
| The "guest" filter is the counterpart of the authentication filters as
| it simply checks that the current user is not logged in. A redirect
| response will be issued if they are, which you may freely change.
|
*/

Route::filter('guest', function()
{
	if (Auth::check()) return Redirect::to('/');
});

/*
|--------------------------------------------------------------------------
| CSRF Protection Filter
|--------------------------------------------------------------------------
|
| The CSRF filter is responsible for protecting your application against
| cross-site request forgery attacks. If this special token in a user
| session does not match the one given in this request, we'll bail.
|
*/

Route::filter('csrf', function()
{
	if (Session::token() != Input::get('_token'))
	{
		throw new Illuminate\Session\TokenMismatchException;
	}
});
