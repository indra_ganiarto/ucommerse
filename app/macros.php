<?php

/*
* List of macro
****************************************************************************************************
Example:
  <ul>
    <li class="{{HTML::activeState('blog')}}">
            <a href="{{ URL::route('blog')}}">Blog</a>
        </li>
    <li class="{{HTML::activeState('about')}}">
            <a href="{{ URL::route('about')}}">About</a>
        </li>
    <li class="{{HTML::activeState('contact')}}">
            <a href="{{ URL::route('contact')}}">Contact</a>
        </li>
  </ul>
****************************************************************************************************
*/
HTML::macro('activeState', function($url)
{
    return Request::is($url) ? 'active' : '';
});


