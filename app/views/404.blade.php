{{-- extend default single layout --}}
@extends('error')
{{--
will be include at @yield('main_content') see login.blade.php
--}}
@section('main_content')
<div class="row">
  <div class="col-md-12">
    <div class="error-container">
      <div class="error-code">404</div>
      <div class="error-text">Page not found</div>
      <div class="error-subtext">Unfortunately we're having trouble loading the page you are looking for. Please wait a moment and try again or use action below.</div>
      <div class="error-actions">
        <div class="row">
          @if(Auth::id()==1)
          <div class="col-md-4">
            <button class="btn btn-warning btn-block btn-lg" onClick="document.location.href = '{{url($admin_url)}}';">Dashboard</button>
          </div>
          <div class="col-md-4">
            <button class="btn btn-info btn-block btn-lg" onClick="document.location.href = '{{url('/')}}';">Home</button>
          </div>
          <div class="col-md-4">
            <button class="btn btn-primary btn-block btn-lg" onClick="history.back();">Previous page</button>
          </div>
          @else
          <div class="col-md-6">
            <button class="btn btn-info btn-block btn-lg" onClick="document.location.href = '{{url('/')}}';">Home</button>
          </div>
          <div class="col-md-6">
            <button class="btn btn-primary btn-block btn-lg" onClick="history.back();">Previous page</button>
          </div>
          @endif
        </div>
      </div>
    </div>
  </div>
</div>
@stop
