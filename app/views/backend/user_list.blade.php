@extends('backend.default')
@section('main_content')
    <div class="page-content">
      <h3 class="page-title">
      {{ $page_title }} <small>{{ $description }}</small>
      </h3>
      <div class="page-bar">
        <ul class="page-breadcrumb">
         <?php
            $list = "";
            for ($i=0; $i < count($brc); $i++) { 
               $list.= "<li>".$brc[$i]."</li>";
            }
         ?>
         {{ $list }}
        </ul>
      </div>
     
      <div class="row">
        <div class="col-md-12">
          <div class="portlet box yellow">
           <div class="portlet-title">
              <div class="caption">
                <i class="fa fa-user"></i>{{ $name }}
              </div>
              <div class="actions">
                <a href="{{ $add_url }}" class="btn btn-default btn-sm">
                <i class="fa fa-pencil"></i> Add </a>
                <div class="btn-group">
                  <a class="btn btn-default btn-sm" href="#" data-toggle="dropdown">
                  <i class="fa fa-cogs"></i> with selected <i class="fa fa-angle-down"></i>
                  </a>
                  <ul class="dropdown-menu pull-right">
                    <li>
                      <a href="#">
                      <i class="fa fa-trash-o"></i> Delete All</a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="portlet-body">
              <div class="table-toolbar">
                
              </div>
              {{ $data }}
            </div>
          </div>
        </div>
      </div>
    </div>

@stop
@section('individual_init_script')
    TableManaged.init();
    $('#checkAll').click(function(event) {  //on click 
        if(this.checked) { // check select status
            $('.checkItem').each(function() { //loop through each checkbox
                this.checked = true;  //select all checkboxes with class "checkbox1"               
            });
        }else{
            $('.checkItem').each(function() { //loop through each checkbox
                this.checked = false; //deselect all checkboxes with class "checkbox1"                       
            });         
        }
    });
@stop
@section('prototype_page_script')

@stop
@section('individual_embed_script')

@stop