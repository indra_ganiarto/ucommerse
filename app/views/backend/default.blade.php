<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
<meta charset="utf-8"/>
<title>{{$page_title}} | {{$com_profile['name']}}</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<meta content="" name="description"/>
<meta content="" name="author"/>
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
<link href="{{asset('themes/backend/metronic')}}/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<link href="{{asset('themes/backend/metronic')}}/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css"/>
<link href="{{asset('themes/backend/metronic')}}/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="{{asset('themes/backend/metronic')}}/assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
<link href="{{asset('themes/backend/metronic')}}/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css"/>
<link href="{{asset('themes/backend/metronic')}}/assets/global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css"/>
<link href="{{asset('themes/backend/metronic')}}/assets/global/plugins/fullcalendar/fullcalendar.min.css" rel="stylesheet" type="text/css"/>
<link href="{{asset('themes/backend/metronic')}}/assets/global/plugins/jqvmap/jqvmap/jqvmap.css" rel="stylesheet" type="text/css"/>
<link href="{{asset('themes/backend/metronic')}}/assets/global/plugins/jquery-nestable/jquery.nestable.css" rel="stylesheet" type="text/css"/>
<link href="{{asset('themes/backend/metronic')}}/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css" rel="stylesheet" type="text/css"/>
<link href="{{asset('themes/backend/metronic')}}/assets/admin/pages/css/tasks.css" rel="stylesheet" type="text/css"/>
<link href="{{asset('themes/backend/metronic')}}/assets/global/plugins/icheck/skins/all.css" rel="stylesheet"/>
<link href="{{asset('themes/backend/metronic')}}/assets/global/css/components.css" id="style_components" rel="stylesheet" type="text/css"/>
<link href="{{asset('themes/backend/metronic')}}/assets/global/css/plugins.css" rel="stylesheet" type="text/css"/>
<link href="{{asset('themes/backend/metronic')}}/assets/admin/layout/css/layout.css" rel="stylesheet" type="text/css"/>
<link href="{{asset('themes/backend/metronic')}}/assets/admin/layout/css/themes/darkblue.css" rel="stylesheet" type="text/css" id="style_color"/>
<link href="{{asset('themes/backend/metronic')}}/assets/admin/layout/css/custom.css" rel="stylesheet" type="text/css"/>
<link href="{{asset('themes/backend/metronic')}}/assets/admin/style.css" rel="stylesheet" type="text/css"/>
<script src="{{asset('themes/backend/metronic')}}/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<link rel="shortcut icon" href="favicon.ico"/>
</head>
<body class="page-header-fixed page-quick-sidebar-over-content page-style-square">
<div class="page-header navbar navbar-fixed-top">
    @include('backend.parts.navbar_top')
</div>
<div class="clearfix">
</div>
<div class="page-container">
    <div class="page-sidebar-wrapper">
        <div class="page-sidebar navbar-collapse collapse">
            @include('backend.parts.sidebar_nav')
        </div>
    </div>
    <div class="page-content-wrapper">
         @yield('main_content')
    </div>
</div>
<div class="page-footer">
    <div class="page-footer-inner">
        {{ date("Y") }} &copy; Ulin Ulin.
    </div>
    <div class="scroll-to-top">
        <i class="icon-arrow-up"></i>
    </div>
</div>
<div id="logout-modal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Confirmation</h4>
            </div>
            <div class="modal-body">
                <p>Are you sure you want to log out?</p>
                <p>Press No if youwant to continue work. Press Yes to logout current user.</p>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn default">No</button>
                <a href="{{url('logout')}}" class="btn green">Yes</a>
            </div>
        </div>
    </div>
</div>

<script src="{{asset('themes/backend/metronic')}}/assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<script src="{{asset('themes/backend/metronic')}}/assets/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
<script src="{{asset('themes/backend/metronic')}}/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="{{asset('themes/backend/metronic')}}/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="{{asset('themes/backend/metronic')}}/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="{{asset('themes/backend/metronic')}}/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="{{asset('themes/backend/metronic')}}/assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<!--<script src="{{asset('themes/backend/metronic')}}/assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>-->
<script src="{{asset('themes/backend/metronic')}}/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<script src="{{asset('themes/backend/metronic')}}/assets/global/plugins/jqvmap/jqvmap/jquery.vmap.js" type="text/javascript"></script>
<script src="{{asset('themes/backend/metronic')}}/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js" type="text/javascript"></script>
<script src="{{asset('themes/backend/metronic')}}/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js" type="text/javascript"></script>
<script src="{{asset('themes/backend/metronic')}}/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js" type="text/javascript"></script>
<script src="{{asset('themes/backend/metronic')}}/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js" type="text/javascript"></script>
<script src="{{asset('themes/backend/metronic')}}/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js" type="text/javascript"></script>
<script src="{{asset('themes/backend/metronic')}}/assets/global/plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js" type="text/javascript"></script>
<script type="text/javascript" src="{{asset('themes/backend/metronic')}}/assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="{{asset('themes/backend/metronic')}}/assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="{{asset('themes/backend/metronic')}}/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<script type="text/javascript" src="{{asset('themes/backend/metronic')}}/assets/global/plugins/jquery-nestable/jquery.nestable.js"></script>

<script src="{{asset('themes/backend/metronic')}}/assets/global/plugins/flot/jquery.flot.min.js" type="text/javascript"></script>
<script src="{{asset('themes/backend/metronic')}}/assets/global/plugins/flot/jquery.flot.resize.min.js" type="text/javascript"></script>
<script src="{{asset('themes/backend/metronic')}}/assets/global/plugins/flot/jquery.flot.categories.min.js" type="text/javascript"></script>
<script src="{{asset('themes/backend/metronic')}}/assets/global/plugins/jquery.pulsate.min.js" type="text/javascript"></script>
<script src="{{asset('themes/backend/metronic')}}/assets/global/plugins/bootstrap-daterangepicker/moment.min.js" type="text/javascript"></script>
<script src="{{asset('themes/backend/metronic')}}/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.js" type="text/javascript"></script>
<script src="{{asset('themes/backend/metronic')}}/assets/global/plugins/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>
<script src="{{asset('themes/backend/metronic')}}/assets/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js" type="text/javascript"></script>
<script src="{{asset('themes/backend/metronic')}}/assets/global/plugins/jquery.sparkline.min.js" type="text/javascript"></script>
<script src="{{asset('themes/backend/metronic')}}/assets/global/plugins/icheck/icheck.min.js"></script>

<script src="{{asset('themes/backend/metronic')}}/assets/global/scripts/metronic.js" type="text/javascript"></script>
<script src="{{asset('themes/backend/metronic')}}/assets/admin/layout/scripts/layout.js" type="text/javascript"></script>
<script src="{{asset('themes/backend/metronic')}}/assets/admin/layout/scripts/quick-sidebar.js" type="text/javascript"></script>
<script src="{{asset('themes/backend/metronic')}}/assets/admin/layout/scripts/demo.js" type="text/javascript"></script>
<script src="{{asset('themes/backend/metronic')}}/assets/admin/pages/scripts/index.js" type="text/javascript"></script>
<script src="{{asset('themes/backend/metronic')}}/assets/admin/pages/scripts/tasks.js" type="text/javascript"></script>
<script src="{{asset('themes/backend/metronic')}}/assets/admin/pages/scripts/table-managed.js"></script>
<script src="{{asset('themes/backend/metronic')}}/assets/admin/pages/scripts/form-icheck.js"></script>

<!--if you want embed script just on individual page-->
@yield('individual_embed_script')

<script>
jQuery(document).ready(function() {  
   //global init  
   Metronic.init();
   Layout.init();
   QuickSidebar.init();

   //individual init
   @yield('individual_init_script')
});

//your own function script
@yield('prototype_page_script')

</script>
</body>
</html>