<ul class="page-sidebar-menu" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
    <li class="sidebar-toggler-wrapper">
        <div class="sidebar-toggler">
        </div>
    </li>
    <li class="heading">
        <h3 class="uppercase">Modules</h3>
    </li>
    <?php
        $allowed = [];
        if(!empty($cur_user_perms) ){
            foreach ($cur_user_perms as $key => $perm) {
                if($perm->name!=null)
                $allowed[] = $perm->name;
             }
        }
    ?>
    @foreach ($sys_menu as $menu)
      {{renderAdminMenu($menu,$allowed)}}
    @endforeach
</ul>