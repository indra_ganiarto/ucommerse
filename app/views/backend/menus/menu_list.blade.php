<?php
  $list = "";
  for ($i=0; $i < count($brc); $i++) { 
     $list.= "<li>".$brc[$i]."</li>";
  }

  $add_perms = $cmd['add'].'_'.$cmd['alias'];
  $delall_perms = $cmd['deleteall'].'_'.$cmd['alias'];

?>
@extends('backend.default')
@section('main_content')
    <div class="page-content">
      <h3 class="page-title">
      {{ $page_title }} <small>{{ $description }}</small>
      </h3>
      <div class="page-bar">
        <ul class="page-breadcrumb">
         {{ $list }}
        </ul>
      </div>
     
      <div class="row">
        <div class="col-md-12">
          <div class="portlet box yellow">
           <div class="portlet-title">
              <div class="caption">
                <i class="fa fa-user"></i>{{ $name }}
              </div>
              <div class="actions">

                @if($cur_user->can($add_perms))
                <a href="{{ $add_url }}" class="btn btn-default btn-sm">
                <i class="fa fa-pencil"></i> Add </a>
                @endif
                
                <div class="btn-group">
                  <a class="btn btn-default btn-sm" href="#" data-toggle="dropdown">
                  <i class="fa fa-cogs"></i> with selected <i class="fa fa-angle-down"></i>
                  </a>
                  <ul class="dropdown-menu pull-right">
                    @if($cur_user->can($delall_perms))
                    <li>
                      <a href="javascript:deleteAll('{{ $delete_all_url }}','{{ $id }}');">
                      <i class="fa fa-trash-o"></i> Delete All</a>
                    </li>
                    @endif
                  </ul>
                </div>
              </div>
            </div>
            <div class="portlet-body">
              <div class="table-toolbar">
                
              </div>

              @if( Session::has('alert_success') )
                <div class="alert alert-success">
                  <a class="close" data-dismiss="alert">&times;</a>
                  {{Session::get('alert_success')}}
                </div>
              @endif

              @if( Session::has('alert_danger') )
                <div class="alert alert-danger">
                  <a class="close" data-dismiss="alert">&times;</a>
                  {{Session::get('alert_danger')}}
                </div>
              @endif
              <form id="{{ $id }}">
                {{ $data }}
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div id="action-modal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title" id="action-modal-title"></h4>
                </div>
                <div class="modal-body" id="action-modal-body"></div>
                <div class="modal-footer">
                    <button type="button" data-dismiss="modal" class="btn default" id="action-modal-dismiss">No</button>
                    <a href="#" class="btn green" id="action-modal-submit">Yes</a>
                </div>
            </div>
        </div>
    </div>
@stop
@section('individual_init_script')
    TableManaged.init();
    setTimeout("$('#checkAll').parent().parent().removeClass('sorting_asc');",100);
    $('#checkAll').click(function(event) { 
        if(this.checked) {
            $('.checkItem').each(function() {
                this.checked = true;             
            });
        }else{
            $('.checkItem').each(function() {
                this.checked = false;             
            });         
        }
    });
@stop
@section('prototype_page_script')
  function deleteRow(url,id){
      var target = url + '/' + id;
      var modal = "#action-modal";
      var modal_title = $(modal+"-title");
      var modal_body = $(modal+"-body");
      var modal_submit = $(modal+"-submit");

      modal_title.html("Confirmation");
      modal_body.html("<p>Are you sure want to delete this row?</p>");
      modal_submit.html("Yes");
      modal_submit.attr("href","javascript:runDelete('" + target + "')");

      $(modal).modal("show");
  }
  function runDelete(target){

      window.location.href=target;

  }
  function runDeleteAll(url,form){
      var modal = "#action-modal";
      var modal_title = $(modal+"-title");
      var modal_body = $(modal+"-body");
      var modal_submit = $(modal+"-submit");
      var modal_dismiss = $(modal+"-dismiss");
      var data = $("#" + form).serialize();

      modal_title.html("Process");
      modal_body.html("<p>Deleting selected data... Please wait...</p>");
      modal_dismiss.hide();
      modal_submit.hide();

      $.post(url,data,function(res){
      
        modal_title.html("Response");
        modal_body.html("<p>"+res.msg+"</p>");
        modal_submit.show();
        modal_submit.html("Ok");
        modal_submit.attr("href","javascript:window.location.reload();");

      },'json');

  }
  function deleteAll(url,form){

      var modal = "#action-modal";
      var modal_title = $(modal+"-title");
      var modal_body = $(modal+"-body");
      var modal_submit = $(modal+"-submit");

      modal_title.html("Confirmation");
      modal_body.html("<p>Are you sure want to delete all this selected row?</p>");
      modal_submit.html("Yes");
      modal_submit.attr("href","javascript:runDeleteAll('" + url + "','" + form + "')");

      $(modal).modal("show");

  }
@stop
@section('individual_embed_script')

@stop