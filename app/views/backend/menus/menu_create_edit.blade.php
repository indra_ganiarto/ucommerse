@extends('backend.default')
@section('main_content')
    <div class="page-content">
      <h3 class="page-title">
      {{ $page_title }} <small>{{ $description }}</small>
      </h3>
      <div class="page-bar">
        <ul class="page-breadcrumb">
         <?php
            $list = "";
            for ($i=0; $i < count($brc); $i++) { 
               $list.= "<li>".$brc[$i]."</li>";
            }
         ?>
         {{ $list }}
        </ul>
      </div>
    
      <div class="row">
        <div class="col-md-12">
          <div class="portlet box yellow">
           <div class="portlet-title">
              <div class="caption">
                <i class="fa fa-user"></i>{{ $name }}
              </div>
              <div class="actions">
                <a href="{{ $back_url }}" class="btn btn-default btn-sm">
                <i class="fa fa-chevron-left"></i> Back </a>
              </div>
            </div>
            <div class="portlet-body">
              @if( Session::has('error') )
              <div class="alert alert-danger">
                <a class="close" data-dismiss="alert">&times;</a>
                {{Session::get('error')}}
              </div>
              @endif

              {{Form::open(['id' =>'menu_form', 'class' => 'form-horizontal', 'url' => $form_url ])}}
              {{Form::hidden('id', $id)}}
                 <div class="form-group">

                      <div class="col-md-6 col-xs-12">
                        <div class="input-group">
                          <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                          <input name="title" type="text" required="required" pattern="[a-zA-Z\s]+" placeholder="Menus Name"
                          class="form-control" title="characters alpha only" value="{{$title}}" />
                        </div>

                        <span id="slug" class="help-block"></span>
                        <input name="slug" type="hidden" class="form-control" value="{{$slug}}" />
                        <span class="help-block">Name of menu, characters alpha only</span>
                      </div>

                    </div>

                    <div class="form-group">
                      <div class="col-md-6 col-xs-12">
                        <textarea name="description" class="form-control" rows="3" placeholder="Description">{{$menu_description}}</textarea>
                        <span class="help-block">Give more description for this menus, max 255 characters</span>
                      </div>
                    </div>

                    @if( count($menu_items) > 0 )

                    <h3 id="nestable-menu">Menu Items
                      <button class="btn btn-default" type="button">
                            <span class="fa fa-plus-square"></span>
                            <span data-action="expand-all">Expand All</span>
                          </button>
                    </h3>
                    <span class="help-block">List of menu item as anchor, <b>Drag</b> <u>menu item</u>  to reorder position. </span>

                    <div class="form-group">
                      <div class="col-md-6 col-xs-12">

                        <div class="dd">
                          <ol class="dd-list">
                            @foreach ($menu_items as $m_item)
                            {{renderMenuItems($m_item)}}
                            @endforeach
                          </ol>

                        </div>

                      </div>
                    </div>

                    @endif


                  </div>
                  <div class="panel-footer">
                    <input type="reset" class="btn btn-default" value="Clear Form" />
                    <input type="submit" class="btn btn-info pull-right" value="Submit" />

                  </div>
              {{Form::close()}}

            </div>
          </div>
        </div>
      </div>
    </div>
    
@stop
@section('individual_init_script')

  $('.dd').nestable({
    maxDepth: 3,
  });

  $('.dd').nestable('collapseAll');

  $('.dd').on('change', function() {
     
     var sdata = window.JSON.stringify($(this).nestable('serialize'));
     var data = { menu : sdata, m_id : {{ $id }} }
     var target = '{{ $update_nested_url }}';
  
     $.post(target,data,function(res){
          window.location.reload();
     });

  });

  $('#nestable-menu').on('click', function(e)
  {
    var target = $(e.target),
    action = target.data('action');

    if (action === 'expand-all') {
      $('.dd').nestable('expandAll');

      target.prev().removeClass('fa-plus-square');
      target.prev().addClass('fa-minus-square');

      target.data('action','collapse-all');
      target.text('Collapse All');

    }
    if (action === 'collapse-all') {
      $('.dd').nestable('collapseAll');
      target.data('action','expand-all');
      target.text('Expand All');

      target.prev().removeClass('fa-minus-square');
      target.prev().addClass('fa-plus-square');
    }

  });
  
@stop
@section('prototype_page_script')

@stop
@section('individual_embed_script')
<script src="{{asset('themes/backend/metronic')}}/assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
@stop