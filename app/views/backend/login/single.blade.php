<!DOCTYPE html>
<html lang="en" class="body-full-height">
<head>
    <!-- META SECTION -->
    <title>{{$title}} | {{$com_profile['name']}}</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <link rel="icon" href="{{asset('themes/backend/metronic')}}/favicon.ico" type="image/x-icon" />
    <!-- END META SECTION -->

    <!-- CSS INCLUDE -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" type="text/css" id="theme" href="{{asset('themes/backend/metronic')}}/assets/global/plugins/font-awesome/css/font-awesome.min.css"/>
    <link rel="stylesheet" type="text/css" id="theme" href="{{asset('themes/backend/metronic')}}/assets/global/plugins/simple-line-icons/simple-line-icons.min.css"/>
    <link rel="stylesheet" type="text/css" id="theme" href="{{asset('themes/backend/metronic')}}/assets/global/plugins/bootstrap/css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" id="theme" href="{{asset('themes/backend/metronic')}}/assets/global/plugins/uniform/css/uniform.default.css"/>
    <link rel="stylesheet" type="text/css" id="theme" href="{{asset('themes/backend/metronic')}}/assets/admin/pages/css/login.css"/>
    <link rel="stylesheet" type="text/css" id="theme" href="{{asset('themes/backend/metronic')}}/assets/global/css/components.css"/>
    <link rel="stylesheet" type="text/css" id="theme" href="{{asset('themes/backend/metronic')}}/assets/global/css/plugins.css"/>
    <link rel="stylesheet" type="text/css" id="theme" href="{{asset('themes/backend/metronic')}}/assets/admin/layout/css/layout.css"/>
    <link rel="stylesheet" type="text/css" id="theme" href="{{asset('themes/backend/metronic')}}/assets/admin/layout/css/themes/darkblue.css"/>
    <link rel="stylesheet" type="text/css" id="theme" href="{{asset('themes/backend/metronic')}}/assets/admin/layout/css/custom.css"/>
    <!-- EOF CSS INCLUDE -->
</head>
<body class="{{$body_css}}">

    @yield('main_content')

    <script src="{{asset('themes/backend/metronic')}}/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
    <script src="{{asset('themes/backend/metronic')}}/assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
    <script src="{{asset('themes/backend/metronic')}}/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="{{asset('themes/backend/metronic')}}/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
    <script src="{{asset('themes/backend/metronic')}}/assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
    <script src="{{asset('themes/backend/metronic')}}/assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
    <!-- END CORE PLUGINS -->
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="{{asset('themes/backend/metronic')}}/assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="{{asset('themes/backend/metronic')}}/assets/global/scripts/metronic.js" type="text/javascript"></script>
    <script src="{{asset('themes/backend/metronic')}}/assets/admin/layout/scripts/layout.js" type="text/javascript"></script>
    <script src="{{asset('themes/backend/metronic')}}/assets/admin/layout/scripts/demo.js" type="text/javascript"></script>
    <script src="{{asset('themes/backend/metronic')}}/assets/admin/pages/scripts/login.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL SCRIPTS -->
    <script>
    jQuery(document).ready(function() {     
        Metronic.init(); // init metronic core components
        Layout.init(); // init current layout
        Login.init();
        Demo.init();
    });
    </script>
</body>
</html>






