@extends('backend.login.single')
@section('main_content')
  <div class="menu-toggler sidebar-toggler">
  </div>
  <div class="logo">
     <h1>UCommerse</h1>
  </div>
  <div class="content">
    {{ Form::open(array('url' => 'admin/authorize','class'=>'login-form')) }}
    
          <h3 class="form-title">Sign In</h3>

          @if(Session::has('error'))
            <div class="alert alert-danger">
               <button class="close" data-close="alert"></button>
              {{Session::get('error')}}
            </div>
          @endif

          <div class="form-group">
              <label class="control-label visible-ie8 visible-ie9">Username</label>
              <input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off" placeholder="Username" name="email"/>
          </div>
          <div class="form-group">
              <label class="control-label visible-ie8 visible-ie9">Password</label>
              <input class="form-control form-control-solid placeholder-no-fix" type="password" autocomplete="off" placeholder="Password" name="password"/>
          </div>
          <div class="form-actions">
              <button type="submit" class="btn btn-success uppercase">Login</button>
              <label class="rememberme check">
              <input type="checkbox" id="remember" name="remember" value="1"/>Remember </label>
              <a href="{{url('users/forgot_password')}}" id="forget-password" class="forget-password">Forgot Password?</a>
          </div>

    {{ Form::close() }}
  </div>
@stop
