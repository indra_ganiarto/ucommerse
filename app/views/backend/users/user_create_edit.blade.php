@extends('backend.default')
@section('main_content')
    <div class="page-content">
      <h3 class="page-title">
      {{ $page_title }} <small>{{ $description }}</small>
      </h3>
      <div class="page-bar">
        <ul class="page-breadcrumb">
         <?php
            $list = "";
            for ($i=0; $i < count($brc); $i++) { 
               $list.= "<li>".$brc[$i]."</li>";
            }
         ?>
         {{ $list }}
        </ul>
      </div>
    
      <div class="row">
        <div class="col-md-12">
          <div class="portlet box yellow">
           <div class="portlet-title">
              <div class="caption">
                <i class="fa fa-user"></i>{{ $name }}
              </div>
              <div class="actions">
                <a href="{{ $back_url }}" class="btn btn-default btn-sm">
                <i class="fa fa-chevron-left"></i> Back </a>
              </div>
            </div>
            <div class="portlet-body">
              @if( Session::has('error') )
              <div class="alert alert-danger">
                <a class="close" data-dismiss="alert">&times;</a>
                {{Session::get('error')}}
              </div>
              @endif

              {{Form::open(['id' =>'user', 'class' => 'form-horizontal', 'url' => $form_url ])}}
                <input type="hidden" name="id" value="{{$id}}">

                <div class="form-group">
                  <label class="control-label col-md-3">Username <span class="required"> * </span></label>
                  <div class="col-md-4">
                    <input value="{{$user->username}}" type="text" name="username" data-required="1" class="form-control" placeholder="Username"/>
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-md-3 control-label">Email Address <span class="required"> * </span></label>
                  <div class="col-md-4">
                    <div class="input-group">
                      <span class="input-group-addon">
                      <i class="fa fa-envelope"></i>
                      </span>
                      <input value="{{$user->email}}" type="email" name="email" class="form-control" placeholder="Email Address">
                    </div>
                  </div>
                </div>
                @if($form_mode=='add')

                 <div class="form-group">
                  <label class="col-md-3 control-label">Password <span class="required"> * </span></label>
                  <div class="col-md-4">
                    <div class="input-group">
                      <span class="input-group-addon">
                      <i class="fa fa-asterisk"></i>
                      </span>
                      <input value="{{$user->email}}" type="password" name="password" class="form-control" placeholder="User Password">
                    </div>
                  </div>
                </div>

                @endif

                @if($form_mode=='edit')

                <div class="form-group">
                    <label class="control-label col-md-3"></label>
                    <div class="col-md-4">
                      <div class="checkbox-list" data-error-container="#user">
                        <label>
                        <input class="icheck" type="checkbox" value="1" name="change_password" id="change_password"/> Change Password? </label>
                      </div>
                      <span class="help-block">
                        To change password check the box then fill current &amp; new password
                      </span>
                    </div>
                </div>
                <div class="form-group" id="new_password">
                  <label class="col-md-3 control-label"><span class="required"> * </span></label>
                  <div class="col-md-4">
                    <div class="input-group">
                      <span class="input-group-addon">
                      <i class="fa fa-asterisk"></i>
                      </span>
                      <input type="password" name="password" class="form-control" placeholder="User Password">
                    </div>
                  </div>
                </div>

                @endif
                
                @if($confirmed==false)

                <div class="form-group">
                    <label class="control-label col-md-3"></label>
                    <div class="col-md-4">
                      <div class="checkbox-list" data-error-container="#user">
                        <label>
                        <input type="checkbox" value="1" name="confirmed" id="confirmed"/> Confirmed </label>
                      </div>
                      <span class="help-block">
                        Set user status to active/confirmed
                      </span>
                    </div>
                </div>

                @endif

                @if($form_mode=='add')

                <div class="form-group">
                    <label class="control-label col-md-3"></label>
                    <div class="col-md-4">
                      <div class="checkbox-list" data-error-container="#user">
                        <label class="check" style="display: block">
                          <input type="checkbox" value="1" name="notify" id="notify"/> Notify 
                        </label>
                      </div>
                      <span class="help-block">
                        Send email notification to user about this account
                      </span>
                    </div>
                </div>

                @endif
                <div class="form-group">
                    <label class="control-label col-md-3">Roles</label>
                    <div class="col-md-4">
                        <span class="help-block">Attach role(s) to this user</span>
                        <div class="input-group">
                          @foreach($roles as $role)
                            <?php
                            $checked ='';
                            if(in_array($role->id, $has_roles_id)){
                              $checked = 'checked="checked"';
                            }
                            ?>
                            @if($role->id==1 && $id!=1)
                              <label class="check" style="display: block">
                                <input disabled="disabled" {{$checked}} type="checkbox" name="roles[]" value="{{$role->id}}"/>
                                {{$role->name}}
                              </label>
                            @else
                              <label class="check" style="display: block">
                                <input type="checkbox" {{$checked}} name="roles[]" value="{{$role->id}}"/>
                                {{$role->name}}
                              </label>
                            @endif
                          @endforeach
                        </div>
                    </div>
                </div>

                <div class="form-actions">
                  <div class="row">
                    <div class="col-md-offset-3 col-md-9">
                      <button type="submit" class="btn green">Submit</button>
                      <button type="reset" class="btn default">Reset</button>
                    </div>
                  </div>
                </div>
              {{Form::close()}}

            </div>
          </div>
        </div>
      </div>
    </div>
    
@stop
@section('individual_init_script')
  $('.icheckbox').iCheck({
      checkboxClass: 'icheckbox_square-grey',
      radioClass: 'iradio_square-grey'
  });
  $('#new_password').hide();
  $('#change_password').on('ifClicked', function(event){
    $('#new_password').slideDown('fast');
  });
  $('#change_password').on('ifUnchecked', function(event){
    $('#new_password').slideUp('fast');
  });
@stop
@section('prototype_page_script')

@stop
@section('individual_embed_script')
<script src="{{asset('themes/backend/metronic')}}/assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
@stop