<div id="header-top">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="header-top-left">
          <ul id="top-links" class="clearfix">
            <li><a href="#" title="My Wishlist">
              <span class="top-icon top-icon-pencil"></span>
              <span class="hide-for-xs">Wishlist</span></a>
            </li>
            <li><a href="#" title="My Account">
              <span class="top-icon top-icon-user"></span>
              <span class="hide-for-xs">Account</span></a>
            </li>
            <li><a href="#" title="My Cart">
              <span class="top-icon top-icon-cart"></span>
              <span class="hide-for-xs">Cart</span></a>
            </li>
            <li><a href="#" title="Checkout">
              <span class="top-icon top-icon-check"></span>
              <span class="hide-for-xs">Checkout</span></a>
            </li>
          </ul>
        </div><!-- End .header-top-left -->
        <div class="header-top-right">
          <div class="header-top-dropdowns pull-right">
            <div class="btn-group dropdown-money">
              <button type="button" class="btn btn-custom dropdown-toggle" data-toggle="dropdown">
                <span class="hide-for-xs">IDR</span><span class="hide-for-lg">$</span>
              </button>
              <ul class="dropdown-menu pull-right" role="menu">
                <li><a href="#"><span class="hide-for-xs">USD</span><span class="hide-for-lg">&euro;</span></a></li>
              </ul>
            </div><!-- End .btn-group -->
            <div class="btn-group dropdown-language">
              <button type="button" class="btn btn-custom dropdown-toggle" data-toggle="dropdown">
                <span class="flag-container"><img src="{{asset('themes/frontend/venedor')}}/images/england-flag.png" alt="flag of england"></span>
                <span class="hide-for-xs">English</span>
              </button>
              <ul class="dropdown-menu pull-right" role="menu">
                <li><a href="#"><span class="flag-container"><img src="{{asset('themes/frontend/venedor')}}/images/italy-flag.png" alt="flag of england"></span><span class="hide-for-xs">Indonesian</span></a></li>
              </ul>
            </div><!-- End .btn-group -->
          </div><!-- End .header-top-dropdowns -->


          <div class="header-text-container pull-right">
            @if(Auth::check())
              <p class="header-text">Welcome, <b>{{ ucfirst(Auth::user()->username) }}</b></p>
            @else
              <p class="header-text">Welcome</p>
            @endif

            <p class="header-link">

              @if(Auth::check())

              @foreach ($cur_user_roles as $role)
                <?php $has_role[] = $role->slug;  ?>
              @endforeach

              @if(in_array('admin',$has_role) || in_array('contributor',$has_role) || in_array('super_admin',$has_role))

              {{-- @if( $cur_user->hasRole("Super Admin") || $cur_user->hasRole("Admin") ) --}}
                <a href="{{url($admin_url)}}">Dashboard</a>&nbsp;
              @endif

                <a href="{{url('logout')}}">Logout</a>
              @else
                <a href="{{url('login')}}">Login</a> or
                <a href="{{url('register')}}">Register</a>
              @endif


            </p>

          </div><!-- End .pull-right -->
        </div><!-- End .header-top-right -->
      </div><!-- End .col-md-12 -->
    </div><!-- End .row -->
  </div><!-- End .container -->
</div><!-- End #header-top -->

<div id="inner-header">
  <div class="container">
    <div class="row">
      <div class="col-md-5 col-sm-5 col-xs-12 logo-container">
        <h1 class="logo clearfix">
          <span>{{$com_profile['name']}}</span>
          <a href="#" title="{{$com_profile['name']}}">
          <img src="{{asset('themes/frontend/venedor')}}/images/logo.png" alt="{{$com_profile['name']}}" width="238" height="76"></a>
        </h1>
      </div><!-- End .col-md-5 -->
      <div class="col-md-7 col-sm-7 col-xs-12 header-inner-right">

        <div class="header-box contact-infos pull-right">
          <ul>
            <li><span class="header-box-icon header-box-icon-skype"></span>venedor_support</li>
            <li><span class="header-box-icon header-box-icon-email"></span><a href="mailto:venedor@gmail.com">venedor@gmail.com</a></li>
          </ul>
        </div><!-- End .contact-infos -->

        <div class="header-box contact-phones pull-right clearfix">
          <span class="header-box-icon header-box-icon-earphones"></span>
          <ul class="pull-left">
            <li>+(404) 158 14 25 78</li>
            <li>+(404) 851 21 48 15</li>
          </ul>
        </div><!-- End .contact-phones -->

      </div><!-- End .col-md-7 -->
    </div><!-- End .row -->
  </div><!-- End .container -->

  <div id="main-nav-container">
    <div class="container">
      <div class="row">
        <div class="col-md-12 clearfix">

          <nav id="main-nav">
            <div id="responsive-nav">
              <div id="responsive-nav-button">
                Menu <span id="responsive-nav-button-icon"></span>
              </div><!-- responsive-nav-button -->
            </div>
            <ul class="menu clearfix">
              <li>
                <a class="active" href="{{url('/')}}">HOME</a>
              </li>
              <li class="mega-menu-container"><a href="#">SHOP</a>
                <div class="mega-menu clearfix">
                  <div class="col-5">
                    <a href="#" class="mega-menu-title">Clothing</a><!-- End .mega-menu-title -->
                    <ul class="mega-menu-list clearfix">
                      <li><a href="#">Dresses</a></li>
                      <li><a href="#">Jeans &amp; Trousers</a></li>
                      <li><a href="#">Blouses &amp; Shirts</a></li>
                      <li><a href="#">Tops &amp; T-Shirts</a></li>
                      <li><a href="#">Jackets &amp; Coats</a></li>
                      <li><a href="#">Skirts</a></li>
                    </ul>
                  </div><!-- End .col-5 -->
                  <div class="col-5">
                    <a href="#" class="mega-menu-title">Shoes</a><!-- End .mega-menu-title -->
                    <ul class="mega-menu-list clearfix">
                      <li><a href="#">Formal Shoes</a></li>
                      <li><a href="#">Casual Shoes</a></li>
                      <li><a href="#">Sandals</a></li>
                      <li><a href="#">Boots</a></li>
                      <li><a href="#">Wide Fit</a></li>
                      <li><a href="#">Slippers</a></li>
                    </ul>
                  </div><!-- End .col-5 -->
                  <div class="col-5">
                    <a href="#" class="mega-menu-title">Accessories</a><!-- End .mega-menu-title -->
                    <ul class="mega-menu-list clearfix">
                      <li><a href="#">Bags &amp; Purses</a></li>
                      <li><a href="#">Belts</a></li>
                      <li><a href="#">Gloves</a></li>
                      <li><a href="#">Jewellery</a></li>
                      <li><a href="#">Sunglasses</a></li>
                      <li><a href="#">Hair Accessories</a></li>
                    </ul>
                  </div><!-- End .col-5 -->
                  <div class="col-5">
                    <a href="#" class="mega-menu-title">Sports</a><!-- End .mega-menu-title -->
                    <ul class="mega-menu-list clearfix">
                      <li><a href="#">Sport Tops &amp; Vests</a></li>
                      <li><a href="#">Swimwear</a></li>
                      <li><a href="#">Footwear</a></li>
                      <li><a href="#">Sports Underwear</a></li>
                      <li><a href="#">Bags</a></li>
                    </ul>
                  </div><!-- End .col-5 -->

                  <div class="col-5">
                    <a href="#" class="mega-menu-title">Maternity</a><!-- End .mega-menu-title -->
                    <ul class="mega-menu-list clearfix">
                      <li><a href="#">Tops &amp; Skirts</a></li>
                      <li><a href="#">Dresses</a></li>
                      <li><a href="#">Trousers &amp; Shorts</a></li>
                      <li><a href="#">Knitwear</a></li>
                      <li><a href="#">Jackets &amp; Coats</a></li>
                    </ul>
                  </div><!-- End .col-5 -->
                </div><!-- End .mega-menu -->
              </li>

              <li>
                <a href="#">PAGES</a>
                <ul>
                  <li><a href="product.html">Product</a></li>
                  <li><a href="cart.html">Cart</a></li>
                  <li><a href="category.html">Category</a></li>
                  <li><a href="blog.html">Blog</a></li>
                  <li><a href="checkout.html">Checkout</a></li>
                  <li><a href="aboutus.html">About Us</a></li>
                  <li><a href="404.html">404 Page</a></li>
                </ul>
              </li>
              <li><a href="contact.html">Contact Us</a></li>
            </ul>

          </nav>

          <div id="quick-access">
            <div class="dropdown-cart-menu-container pull-right">
              <div class="btn-group dropdown-cart">
                <button type="button" class="btn btn-custom dropdown-toggle" data-toggle="dropdown">
                  <span class="cart-menu-icon"></span>
                  0 item(s) <span class="drop-price">- $0.00</span>
                </button>

                <div class="dropdown-menu dropdown-cart-menu pull-right clearfix" role="menu">
                  <p class="dropdown-cart-description">Recently added item(s).</p>
                  <ul class="dropdown-cart-product-list">
                    <li class="item clearfix">
                      <a href="#" title="Delete item" class="delete-item"><i class="fa fa-times"></i></a>
                      <a href="#" title="Edit item" class="edit-item"><i class="fa fa-pencil"></i></a>
                      <figure>
                        <a href="product.html"><img src="{{asset('themes/frontend/venedor')}}/images/products/thumbnails/item12.jpg" alt="phone 4"></a>
                      </figure>
                      <div class="dropdown-cart-details">
                        <p class="item-name">
                          <a href="product.html">Cam Optia AF Webcam </a>
                        </p>
                        <p>
                          1x
                          <span class="item-price">$499</span>
                        </p>
                      </div><!-- End .dropdown-cart-details -->
                    </li>
                    <li class="item clearfix">
                      <a href="#" title="Delete item" class="delete-item"><i class="fa fa-times"></i></a>
                      <a href="#" title="Edit item" class="edit-item"><i class="fa fa-pencil"></i></a>
                      <figure>
                        <a href="product.html"><img src="{{asset('themes/frontend/venedor')}}/images/products/thumbnails/item13.jpg" alt="phone 2"></a>
                      </figure>
                      <div class="dropdown-cart-details">
                        <p class="item-name">
                          <a href="product.html">Iphone Case Cover Original</a>
                        </p>
                        <p>
                          1x
                          <span class="item-price">$499<span class="sub-price">.99</span></span>
                        </p>
                      </div><!-- End .dropdown-cart-details -->
                    </li>
                  </ul>

                  <ul class="dropdown-cart-total">
                    <li><span class="dropdown-cart-total-title">Shipping:</span>$7</li>
                    <li><span class="dropdown-cart-total-title">Total:</span>$1005<span class="sub-price">.99</span></li>
                  </ul><!-- .dropdown-cart-total -->
                  <div class="dropdown-cart-action">
                    <p><a href="#" class="btn btn-custom-2 btn-block">Cart</a></p>
                    <p><a href="#" class="btn btn-custom btn-block">Checkout</a></p>
                  </div><!-- End .dropdown-cart-action -->

                </div><!-- End .dropdown-cart -->
              </div><!-- End .btn-group -->
            </div><!-- End .dropdown-cart-menu-container -->


            <form class="form-inline quick-search-form" role="form" action="#">
              <div class="form-group">
                <input type="text" class="form-control" placeholder="Search here">
              </div><!-- End .form-inline -->
              <button type="submit" id="quick-search" class="btn btn-custom"></button>
            </form>
          </div><!-- End #quick-access -->
        </div><!-- End .col-md-12 -->
      </div><!-- End .row -->
    </div><!-- End .container -->

  </div><!-- End #nav -->
</div><!-- End #inner-header -->
