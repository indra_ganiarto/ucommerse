<div class="widget banner-slider-container">
  <div class="banner-slider flexslider">
    <ul class="banner-slider-list clearfix">
      <li><a href="#"><img src="{{asset('themes/frontend/venedor')}}/images/banner1.jpg" alt="Banner 1"></a></li>
      <li><a href="#"><img src="{{asset('themes/frontend/venedor')}}/images/banner2.jpg" alt="Banner 2"></a></li>
      <li><a href="#"><img src="{{asset('themes/frontend/venedor')}}/images/banner3.jpg" alt="Banner 3"></a></li>
    </ul>
  </div>
</div><!-- End .widget -->
