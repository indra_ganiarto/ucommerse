<!DOCTYPE html>
<!--[if IE 8]> <html class="ie8"> <![endif]-->
<!--[if IE 9]> <html class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <title>{{$page_title}} | {{$com_profile['name']}}</title>
  <meta name="author" content="{{$page_meta['author']}}">
  <meta name="keyword" content="{{$page_meta['keyword']}}">
  <meta name="description" content="{{$page_meta['description']}}">
  <!--[if IE]> <meta http-equiv="X-UA-Compatible" content="IE=edge"> <![endif]-->
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <link href='//fonts.googleapis.com/css?family=PT+Sans:400,700,400italic,700italic%7CPT+Gudea:400,700,400italic%7CPT+Oswald:400,700,300' rel='stylesheet' id="googlefont">

  <link rel="stylesheet" href="{{asset('themes/frontend/venedor')}}/css/bootstrap.min.css">
  <link rel="stylesheet" href="{{asset('themes/frontend/venedor')}}/css/font-awesome.min.css">
  <link rel="stylesheet" href="{{asset('themes/frontend/venedor')}}/css/prettyPhoto.css">
  <link rel="stylesheet" href="{{asset('themes/frontend/venedor')}}/css/revslider.css">
  <link rel="stylesheet" href="{{asset('themes/frontend/venedor')}}/css/owl.carousel.css">
  <link rel="stylesheet" href="{{asset('themes/frontend/venedor')}}/css/style.css">
  <link rel="stylesheet" href="{{asset('themes/frontend/venedor')}}/css/responsive.css">

  <!-- Favicon and Apple Icons -->
  <link rel="icon" type="image/png" href="{{asset('themes/frontend/venedor')}}/images/icons/icon.png">
  <link rel="apple-touch-icon" sizes="57x57" href="{{asset('themes/frontend/venedor')}}/images/icons/apple-icon-57x57.png">
  <link rel="apple-touch-icon" sizes="72x72" href="{{asset('themes/frontend/venedor')}}/images/icons/apple-icon-72x72.png">

  <!--- jQuery -->
  {{-- <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script> --}}
  <script src="{{asset('themes/frontend/venedor')}}/js/jquery-1.11.1.min.js"></script>

    <!--[if lt IE 9]>
      <script src="{{asset('themes/frontend/venedor')}}/js/html5shiv.js"></script>
      <script src="{{asset('themes/frontend/venedor')}}/js/respond.min.js"></script>
      <![endif]-->

      <style id="custom-style">

      </style>
    </head>
    <body>
      <div id="wrapper">
        <header id="header">

          @include('frontend.venedor.parts.header')

        </header><!-- End #header -->

        <section id="content">

        @yield('main-content')

        </div><!-- End .col-md-9 -->

        <div class="col-md-3 col-sm-4 col-xs-12 sidebar">

        @yield('sidebar')

        </div><!-- End .col-md-3 -->
      </div><!-- End .row -->


      <!-- Start #brand-slider-container -->
      {{-- @include('frontend.venedor.brand') --}}
      <!-- End #brand-slider-container -->

    </div><!-- End .col-md-12 -->
  </div><!-- End .row -->
</div><!-- End .container -->

</section><!-- End #content -->

<footer id="footer">
  @include('frontend.venedor.parts.footer')
</footer><!-- End #footer -->
</div><!-- End #wrapper -->

<a href="#" id="scroll-top" title="Scroll to Top"><i class="fa fa-angle-up"></i></a><!-- End #scroll-top -->

<!-- END -->
<script src="{{asset('themes/frontend/venedor')}}/js/bootstrap.min.js"></script>
<script src="{{asset('themes/frontend/venedor')}}/js/smoothscroll.js"></script>
<script src="{{asset('themes/frontend/venedor')}}/js/jquery.debouncedresize.js"></script>
<script src="{{asset('themes/frontend/venedor')}}/js/retina.min.js"></script>
<script src="{{asset('themes/frontend/venedor')}}/js/jquery.placeholder.js"></script>
<script src="{{asset('themes/frontend/venedor')}}/js/jquery.hoverIntent.min.js"></script>
{{-- <script src="{{asset('themes/frontend/venedor')}}/js/twitter/jquery.tweet.min.js"></script> --}}

<script src="{{asset('themes/frontend/venedor')}}/js/jquery.flexslider-min.js"></script>
<script src="{{asset('themes/frontend/venedor')}}/js/owl.carousel.min.js"></script>
<script src="{{asset('themes/frontend/venedor')}}/js/jflickrfeed.min.js"></script>
<script src="{{asset('themes/frontend/venedor')}}/js/jquery.prettyPhoto.js"></script>
<script src="{{asset('themes/frontend/venedor')}}/js/jquery.themepunch.tools.min.js"></script>
<script src="{{asset('themes/frontend/venedor')}}/js/jquery.themepunch.revolution.min.js"></script>

<script type="text/javascript">
  var twitter_username = "azulkipli";
  var theme_path = "{{asset('themes/frontend/venedor')}}";
</script>

<script src="{{asset('themes/frontend/venedor')}}/js/main.js"></script>

<script>
  $(function() {
            // Slider Revolution
            jQuery('#slider-rev').revolution({
              delay:8000,
              startwidth:1170,
              startheight:600,
              onHoverStop:"true",
              hideThumbs:250,
              navigationHAlign:"center",
              navigationVAlign:"bottom",
              navigationHOffset:0,
              navigationVOffset:20,
              soloArrowLeftHalign:"left",
              soloArrowLeftValign:"center",
              soloArrowLeftHOffset:0,
              soloArrowLeftVOffset:0,
              soloArrowRightHalign:"right",
              soloArrowRightValign:"center",
              soloArrowRightHOffset:0,
              soloArrowRightVOffset:0,
              touchenabled:"on",
              stopAtSlide:-1,
              stopAfterLoops:-1,
              dottedOverlay:"none",
              fullWidth:"on",
              spinned:"spinner5",
              shadow:0,
              hideTimerBar: "on",
                // navigationStyle:"preview4"
              });

          });
</script>
</body>
</html>
