@extends('frontend.venedor.layout.nosidebar')

@section('main-content')

<div id="breadcrumb-container">
  <div class="container">
    <ul class="breadcrumb">
      <li><a href="index.html">Home</a></li>
      <li class="active">Login</li>
    </ul>
  </div>
</div>

<div class="container">
  <div class="row">
    <div class="col-md-12">
      <header class="content-title">
        <h1 class="title">Login or Create Account</h1>
        <div class="md-margin"></div><!-- space -->
      </header>
      <div class="row">
        <div class="col-md-4 col-sm-4 col-xs-12">
          <h2>New Customer</h2>
          <p>By creating an account with our store, you will be able to move through the checkout process faster, store multiple shipping addresses, view and track your orders in your account and more.</p>
          <div class="md-margin"></div><!-- space -->
          <a href="{{url('register')}}" class="btn btn-custom-2">Create Account</a>
          <div class="lg-margin"></div><!-- space -->
        </div><!-- End .col-md-6 -->
        <div class="col-md-8 col-sm-8 col-xs-12">
          <h2>Registered Customers</h2>
          <p>If you have an account with us, please log in.</p>
          <div class="xs-margin"></div>

          @if(Session::has('error'))

          <div class="alert alert-danger">
            <a class="close" data-dismiss="alert">&times;</a>
            {{Session::get('error')}}
          </div>

          <div class="xs-margin"></div>

          @endif


          {{ Form::open(array('url' => 'authorize','class'=>'form-horizontal')) }}

            <div class="input-group">
              <span class="input-group-addon"><span class="input-icon input-icon-email"></span><span class="input-text">Email&#42;</span></span>
              <input name="email" type="text" required class="form-control input-lg" placeholder="Your Email">
            </div><!-- End .input-group -->
            <div class="input-group xs-margin">
              <span class="input-group-addon"><span class="input-icon input-icon-password"></span><span class="input-text">Password&#42;</span></span>
              <input name="password" type="password" required class="form-control input-lg" placeholder="Your Password">
            </div><!-- End .input-group -->
            <span class="help-block text-right"><a href="{{url('forgot-password')}}">Forgot password?</a></span>
            <button class="btn btn-custom-2">LOGIN</button>

          {{Form::close()}}

          <div class="sm-margin"></div><!-- space -->
        </div><!-- End .col-md-6 -->
      </div><!-- End.row -->
    </div><!-- End .col-md-12 -->
  </div><!-- End .row -->
</div><!-- End .container -->

@stop
