<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Company Settings
	|--------------------------------------------------------------------------
	|
	*/

	'system' => [
		'admin_url' => 'admin'
	],

	'profile' => [
		'name'  => 'Larulin',
		'email' => 'azul@ulinulin.com',
	],

	'link' => [
		'facebook' => 'https://facebook.com/azulkipli',
		'twitter'  => 'https://twitter.com/azulkipli',
	],

	'meta' => [
		'author'      => 'Indra Ganiarto',
		'keyword'     => 'ulinulin, ecommerce, website, platform',
		'description' => 'website application platform for Ulinulin',
	],

];
