<?php

// mailcatcher local mail testing
return [
    'driver' => 'smtp',
    'host' => '127.0.0.1',
    'port' => 1025,
    'encryption' => '',
    'from' => array('address' => Config::get('company.profile.email') , 'name' => Config::get('company.profile.name')),
];
