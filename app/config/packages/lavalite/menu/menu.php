<?php

return array(
    'name'              => 'Menu',
    'model'             => 'Lavalite\Menu\Models\Menu',
    'permissions'       => ['admin'     => ['view', 'create', 'edit', 'delete', 'image']],

    'fillable'          => ['parent_id','name','url','order','status', 'description','has_sub','type','open','key','icon'],
    'translatable'      => ['name', 'description'],
    'upload-folder'     => '/packages/lavalite/menu/menu/',
    'uploadable'        => ['single' => [], 'multiple' => []],
    'table'             => 'menus',
);
