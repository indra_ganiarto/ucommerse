<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Menu extends \Eloquent {
  /**
     * The database table used by the model.
     *
     * @var string
     */
    use SoftDeletingTrait;

    protected $table = 'menu';

    protected $fillable = ['title','slug','description'];


    protected $dates = ['deleted_at'];



    public function items() {

        return $this->hasMany('MenuItem', 'm_id', 'id');

    }

}
