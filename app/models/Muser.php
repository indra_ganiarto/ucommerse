<?php

class Muser{

    public static function getUser($params){
    	
    	$params['key'] = "id";
    	$table = "users";
    	$row_item = Muser::setRowItem($params);
  		 	
    	$fields = array('id','username',
		        
	        DB::raw($row_item['action']),
	        DB::raw($row_item['checkbox'])
	      
	    );
			   
  		return DB::table($table)
			    ->select($fields);
         
    }
    public static function setRowItem($params){
     
    	 $edit_perms = $params['edit'].'_'.$params['alias'];
  		 $del_perms = $params['delete'].'_'.$params['alias'];
  		 $cur_user = app('cache_user');

  		 $action = null;
  		 $separator = null;
  		 $concat_action = null;
  		 $concat_checkbox = "concat('<center><input type=\"checkbox\" class=\"checkItem\" id=\"s_',".$params['key'].",'\" name=\"s_',".$params['key'].",'\"></center>') AS checkbox";
  		 
  		 if($cur_user->can($edit_perms)){
  		 	$action.="'<a href=\"".$params['edit_url']."',".$params['key'].",'\"  class=\"btn btn-xs btn-default\"><i class=\"fa fa-pencil\"></i></a>'";
  		 }
  		 if($cur_user->can($del_perms)){
  		 	if(!empty($action)){
  		 		$separator=",";
  		 	}
  		 	$action.=$separator."'<a href=\"javascript:deleteRow(''".$params['delete_url']."'',&#39;',".$params['key'].",'&#39;);\" class=\"btn btn-xs btn-danger\"><i class=\"fa fa-trash\"></i></a>'";
  		 }
  		 if(!empty($action)){
  		 	$concat_action = "concat(".$action.") as action";
  		 }else{
  		 	$concat_action = "'' as action";
  		 }

  		 return [
  		 	"action" => $concat_action,
  		 	"checkbox" => $concat_checkbox
  		 ];
    
    }
   
}