<?php

class DashboardController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /admin
	 *
	 * @return Response
	 */
	public function index()
	{
		//
		if(Auth::check()){
			//Debugbar::info(Cache::get('cur_user'));
			$page_title  = 'Dashboard';
    		return View::make('backend.dashboard.index', compact('page_title'));

		}else{
			$page['title'] = 'Login';
			$page['body_css'] = 'login';
			$page_meta = Config::get('company.meta');
			return View::make('backend.login.login', $page);
		}

	}


	/**
	 * Display a listing of the resource.
	 * GET /admin/help
	 *
	 * @return Response
	 */
	public function help()
	{

		$page_title = 'Help';
		$page_meta = Config::get('company.meta');
		return View::make('backend.help', compact('page_title'));

	}

	/**
	 * Load login form for frontend
	 *
	 * @return Response
	 */
	public function login()
	{
		//

	}

	/**
     * Attempt to do login
     *
     * @return  Illuminate\Http\Response
     */
	public function authorize()
	{
		$repo = App::make('UserRepository');
		$input = Input::all();

		if ($repo->login($input)) {

			return Redirect::to('admin');

		} else {

			if ($repo->isThrottled($input)) {
				$err_msg = Lang::get('confide::confide.alerts.too_many_attempts');
			} elseif ($repo->existsButNotConfirmed($input)) {
				$err_msg = Lang::get('confide::confide.alerts.not_confirmed');
			} else {
				$err_msg = Lang::get('confide::confide.alerts.wrong_credentials');
			}

			return Redirect::to('admin')
			->withInput(Input::except('password'))
			->with('error', $err_msg);

		}
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /dashboard/create
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /dashboard
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 * GET /dashboard/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /dashboard/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /dashboard/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /dashboard/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
