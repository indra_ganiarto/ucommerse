<?php

class RoleController extends Controller {

	/**
	 * Display a listing of the resource.
	 * GET /role
	 *
	 * @return Response
	 */
	public function index()
	{
		//
		$expiresAt = Carbon::now()->addMinutes(10);

		if(Cache::has('roles')){
			$roles = Cache::get('roles');
		}else{
			Cache::put('roles', Role::with('perms')->get(), $expiresAt);
			$roles = Cache::get('roles');
		}

		$page_title = 'Roles';
		return View::make('backend.role_list', compact('page_title','roles'));
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /role/create
	 *
	 * @return Response
	 */
	public function create()
	{
		$expiresAt = Carbon::now()->addMinutes(10);

		if(Cache::has('permissions')){
			$permissions = Cache::get('permissions');
		}else{
			Cache::put('permissions', Permission::all(), $expiresAt);
			$permissions = Cache::get('permissions');
		}

		$vars = [
			'page_title'   => 'Add Roles',
			'display_name' => '',
	    'description'  => '',
	    'id'  				 => '',
	    'permissions'  => $permissions,
	    'has_perms'  	 => array(),
			'slug' 				 => '',
	    'form_url'     => app('admin_url').'/system/roles/create',
		];

		return View::make('backend.role_create_edit', $vars);

	}

	/**
	 * Store a newly created resource in storage.
	 * POST /role
	 *
	 * @return Response
	 */
	public function store()
	{
		//
		$data = Input::all();
		$link_create = app('admin_url').'/system/roles/create';
		$link_list = app('admin_url').'/system/roles';

		if(empty($data['display_name']) || empty($data['slug'])){
			return Redirect::to($link_create)->with('error','Role name required');
		}

		if(Role::where('slug', '=', $data['slug'])->exists()){
			return Redirect::to($link_create)->with('error','Role name exists try another name');

		}

		$role              = new Role;
		$role->name        = $data['display_name'];
		$role->slug        = $data['slug'];
		$role->description = trim($data['description']);

		if($role->save() && $role->perms()->sync($data['permissions']) ){
			return Redirect::to($link_list)->with('message','Role added');
		}else{
			return Redirect::to($link_create)->with('error','Add role failed');
		}
	}

	/**
	 * Display the specified resource.
	 * GET /role/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /role/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
		$role        = Role::with('perms')->findOrFail($id);
		$permissions = Permission::all();

		$has_perms = array();
		foreach ($role->perms as $key => $value) {
			$has_perms[] = $value->id;
		}

		// dd($role);
		$vars = [
			'page_title'   => 'Edit Roles',
			'display_name' => $role->name,
			'slug' 				 => $role->slug,
			'description'  => $role->description,
			'id'           => $role->id,
			'role'         => $role,
	    'permissions'  => $permissions,
	    'has_perms'  	 => $has_perms,
			'form_url'     => app('admin_url').'/system/roles/update',
		];

		return View::make('backend.role_create_edit', $vars);
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /role/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update()
	{
		//
		$data = Input::all();
		$link_edit = app('admin_url').'/system/roles/edit/'.$data['id'];
		$link_list = app('admin_url').'/system/roles';

		if(empty($data['display_name']) || empty($data['slug'])){
			return Redirect::to($link_edit)->with('error','Role name required');
		}

	  $role = Role::findOrFail($data['id']);

		$role->name        = $data['display_name'];
		$role->slug        = $data['slug'];
		$role->description = trim($data['description']);

		if($role->save() && $role->perms()->sync($data['permissions']) ){

			return Redirect::to($link_list)->with('message','Role updated');

		}else{
			return Redirect::to($link_edit)->with('error','Update role failed');
		}

	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /role/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
		$role = Role::findOrFail($id);
		$link_list = app('admin_url').'/system/roles';
		if($role->delete()){
			return Redirect::to($link_list)->with('message','Role deleted');
		}
	}

}
