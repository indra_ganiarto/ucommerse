<?php

class FrontController extends Controller {

	/**
	 * Display a listing of the resource.
	 * GET /front
	 *
	 * @return Response
	 */
	public function index()
	{
		//
		$page_title = 'Home';

		$page_meta = Config::get('company.meta');

		return View::make('frontend.venedor.index', compact('page_title','page_meta'));
	}


	public function login()
	{
		//
		if(Auth::check()){
			return Redirect::to('/');
		}

		$page_title = 'Login - Customer';
		$page_meta = Config::get('company.meta');

		return View::make('frontend.venedor.login', compact('page_title','page_meta'));
	}

	public function register()
	{
		//
		$page_title = 'Register - Customer';
		$page_meta = Config::get('company.meta');

		return View::make('frontend.venedor.register', compact('page_title','page_meta'));
	}


	/**
     * Attempt to do login
     *
     * @return  Illuminate\Http\Response
     */
	public function authorize()
	{
		$repo = App::make('UserRepository');
		$input = Input::all();

		if ($repo->login($input)) {

			return Redirect::to('/');

		} else {

			if ($repo->isThrottled($input)) {
				$err_msg = Lang::get('confide::confide.alerts.too_many_attempts');

			}
			if($repo->existsButNotConfirmed($input) ) {
				$err_msg = Lang::get('confide::confide.alerts.not_confirmed');

			} else {
				$err_msg = Lang::get('confide::confide.alerts.wrong_credentials');

			}

			return Redirect::to('login')
			->withInput(Input::except('password'))
			->with('error', $err_msg);

		}
	}

	/**
     * Log the user out of the application.
     *
     * @return  Illuminate\Http\Response
     */
	public function logout()
	{
		Confide::logout();

		return Redirect::to('/');
	}


	/**
	 * Show the form for creating a new resource.
	 * GET /front/create
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /front
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 * GET /front/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /front/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /front/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /front/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
