<?php

class PermissionController extends Controller {

	/**
	 * Display a listing of the resource.
	 * GET /permission
	 *
	 * @return Response
	 */
	public function index()
	{
		//

		$expiresAt = Carbon::now()->addMinutes(10);

		if(Cache::has('permissions')){
			$permissions = Cache::get('permissions');
		}else{
			Cache::put('permissions', Permission::all(), $expiresAt);
			$permissions = Cache::get('permissions');
		}

		$page_title  = 'Permissions';

		return View::make('backend.permission_list', compact('page_title','permissions'));
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /permission/create
	 *
	 * @return Response
	 */
	public function create()
	{

		$vars = [
			'page_title'  => 'Add Permissions',
			'display_name' => '',
	    'description'  => '',
	    'id'  => '',
	    'form_url'     => app('admin_url').'/system/permissions/create',
		];

		return View::make('backend.permission_create_edit', $vars);

	}

	/**
	 * Store a newly created resource in storage.
	 * POST /permission
	 *
	 * @return Response
	 */
	public function store()
	{
		//
		$data = Input::all();

		$link_create = app('admin_url').'/system/permissions/create';
		$link_list = app('admin_url').'/system/permissions';

		if(empty($data['display_name']) || empty($data['slug'])){
			return Redirect::to($link_create)->with('error','Permission name required');
		}

		if(Permission::where('name', '=', $data['slug'])->exists()){
			return Redirect::to($link_create)->with('error','Permission name exists try another name');

		}

		$permission               = new Permission;
		$permission->display_name = $data['display_name'];
		$permission->name         = $data['slug'];
		$permission->description  = trim($data['description']);

		if($permission->save()){
			return Redirect::to($link_list)->with('message','Permission added');
		}else{
			return Redirect::to($link_create)->with('error','Add role failed');
		}
	}

	/**
	 * Display the specified resource.
	 * GET /permission/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /permission/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
		$permission = Permission::findOrFail($id);
		// dd($permission);
		$vars = [
			'page_title'   => 'Edit Permissions',
			'display_name' => $permission->display_name,
			'description'  => $permission->description,
			'id'           => $permission->id,
			'role'         => $permission,
			'form_url'     => app('admin_url').'/system/permissions/update',
		];

		return View::make('backend.permission_create_edit', $vars);
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /permission/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update()
	{
		//
		$data = Input::all();

		$link_edit = app('admin_url').'/system/permissions/edit/'.$data['id'];

		if(empty($data['display_name']) || empty($data['slug'])){
			return Redirect::to($link_edit)->with('error','Permission name required');
		}

	  $permission = Permission::findOrFail($data['id']);

		$permission->display_name = $data['display_name'];
		$permission->name         = $data['slug'];
		$permission->description  = trim($data['description']);

		if($permission->save()){
			return Redirect::to(app('admin_url').'/system/permissions')->with('message','Permission updated');
		}else{
			return Redirect::to(app('admin_url').'/system/permissions/edit/'.$data['id'])->with('error','Update permission failed');
		}

	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /permission/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
		$permission = Permission::findOrFail($id);
		// dd($permission);
		if($permission->delete()){
			return Redirect::to(app('admin_url').'/system/permissions')->with('message','Permission deleted');
		}
	}

	public function assign(){
		$roles       = Role::all();
		$permissions = Permission::all();

		$vars = [
			'page_title'  => 'Edit Roles',
			'roles'       => $roles,
			'permissions' => $permissions ,
			'form_url'    => app('admin_url').'/system/roles/assign',
		 ];

		 return View::make('backend.assign_permissions', $vars);
	}

	public function bulk_assign(){
		// dd(Input::all());
		$data = Input::all();
		$tmp_roles = explode('',$data['roles[]']);
		dd($tmp_roles);
	}

}
