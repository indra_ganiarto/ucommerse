<?php
use Muser;
use Datatable;
class UsersController extends Controller
{
    var $params;

    function __construct(){

        $this->params['alias'] = "user";
        $this->params['add'] = "c";
        $this->params['edit'] = "e";
        $this->params['delete'] = "d";
        $this->params['deleteall'] = "da";

        $this->params['edit_url'] = URL::to('/').'/admin/system/users/edit';
        $this->params['delete_url'] = URL::to('/').'/admin/system/users/delete';

    }
    public function index(){  

        $breadcrumb = array(
                            "<i class='icon-settings'></i><a href='#'>System</a><i class='fa fa-angle-right'></i>",
                            "<a href='#'>Users</a><i class='fa fa-angle-right'></i>",
                            "<a href='#'>User List</a></i>"
                            );
        $vars = [
            'page_title'        => 'Manage User',
            'name'              => 'User List',
            'description'       => 'Manage System Apps User Login',
            'id'                => 'user-list',
            'cmd'               => $this->params,
            'add_url'           => URL::to('/').'/admin/system/users/create',
            'delete_all_url'    => URL::to('/').'/admin/system/users/deleteall',
            'brc'               => $breadcrumb,
            'data'              => $this->user_render_datatable(),
            'has_roles'         => array()
        ];
        Debugbar::info(Cache::get('cur_user'));

        return View::make('backend.users.user_list', $vars);

    }
    public function user_source(){
    
        $data = \Muser::getUser($this->params);
        return Datatable::query($data)
        ->showColumns('checkbox','id', 'username','action')
        ->searchColumns('username')
        ->orderColumns('id','username')
        ->make();
        

    }

    public function user_render_datatable(){

        $html = Datatable::table()
                    ->addColumn('<center><input type="checkbox" id="checkAll"></center>','id','Username','Action')
                    ->addColumnWidth('40px','100px','','100px')
                    ->setUrl(route('users.datatable.list'))
                    ->render();
        return $html;
    }
    public function create()
    {
        return View::make(Config::get('confide::signup_form'));

    }

    public function add(){

         $breadcrumb = array(
                            "<i class='icon-settings'></i><a href='#'>System</a><i class='fa fa-angle-right'></i>",
                            "<a href='#'>Users</a><i class='fa fa-angle-right'></i>",
                            "<a href='#'>Add New User</a></i>"
                            );

        $expiresAt = Carbon::now()->addMinutes(10);

        if(Cache::has('roles')){
            $roles = Cache::get('roles');
        }else{
            Cache::put('roles', Role::with('perms')->get(), $expiresAt);
            $roles = Cache::get('roles');
        }

        $vars = [
            'page_title'  => 'User Manage',
            'name'        => 'Add New User',
            'description' => 'Adding new sys user',
            'id'          => '',
            'form_mode'   => 'add',
            'back_url'     => URL::to('/').'/admin/system/users',
            'brc'         => $breadcrumb,
            'roles'       => $roles,
            'user'        => new User,
            'confirmed'   => 0,
            'has_roles_id'   => array(),
            'form_url'    => app('admin_url').'/system/users/create',
        ];

        return View::make('backend.users.user_create_edit', $vars);
    }

    public function p_create(){
        $data = Input::all();

        $link_create = app('admin_url').'/system/users/create';
        $link_list= app('admin_url').'/system/users';

        if(empty($data['username']) && empty($data['email'])  ){
            return Redirect::to($link_create)
            ->withInput(Input::except('password'))
            ->with('error', 'You must fill username & email');
        }

        if(User::where('email', '=', $data['email'])->exists()){
            return Redirect::to($link_create)->with('error','Username/email exists try another name/email');
        }

        $user                    = new User;
        $user->username          = $data['username'];
        $user->email             = $data['email'];
        $user->password          = $data['password'];
        $user->password_confirmation         = $data['password'];
        $user->confirmation_code = md5(uniqid(mt_rand(), true));

        if(!empty($data['confirmed'])){
            $user->confirmed = 1;
        }

        // dd($user);

        if($user->save() ){

            $message = 'User created successfully!!';

            if(!empty($data['roles'])){
                foreach($data['roles'] as $rid){
                    $user->roles()->attach($rid);
                }
                $message += '<br>with role attached';
            }

            if(!empty($data['confirmed'])){
                $message += '<br>status confimed';
            }

            // Send email to user if notify checked
            if(!empty($data['notify'])){

                if(Mail::queueOn(
                    Config::get('confide::email_queue'),
                    Config::get('confide::email_account_confirmation'),
                    compact('user'),
                    function ($message) use ($user) {
                        $message
                            ->to($user->email, $user->username)
                            ->subject(Lang::get('confide::confide.email.account_confirmation.subject'));
                    }
                )){
                    $message += "Notification mail sent to user";
                }else{
                    $message += "Sent notification email failed";
                }
            }

            return Redirect::to($link_list)->withInput(Input::except('password'))
            ->with('alert_success',$message);

        }else{
            return Redirect::to($link_create)->withInput(Input::except('password'))
            ->with('error','Create user failed');
        }

    }
    public function edit($id)
    {

        $user = User::findOrFail($id);
        $roles = Role::all();
        $roles_id = Role::all()->lists('id');
        $breadcrumb = array(
                            "<i class='icon-settings'></i><a href='#'>System</a><i class='fa fa-angle-right'></i>",
                            "<a href='#'>Users</a><i class='fa fa-angle-right'></i>",
                            "<a href='#'>Edit User</a></i>"
                            );
        $vars = [
            'page_title'   => 'Manage Users',
            'name'         => 'Edit User',
            'description'  => 'Editing Data User',
            'id'           => $user->id,
            'brc'          => $breadcrumb,
            'confirmed'    => $user->confirmed,
            'user'         => $user,
            'back_url'     => URL::to('/').'/admin/system/users',
            'roles'        => $roles,
            'has_roles'    => $user->roles,
            'has_roles_id' => $user->roles->lists('id'),
            'form_url'     => app('admin_url').'/system/users/update',
            'form_mode'    => 'edit'
        ];

        return View::make('backend.users.user_create_edit', $vars);

    }

    public function p_update(){
        $data = Input::all();
        //return var_dump($data);
        $roles_id = Role::all()->lists('id');
        $link_edit = URL::to('/').'/admin/system/users/edit/'.$data['id'];
        $link_list = URL::to('/').'/admin/system/users';

        if(empty($data['username']) && empty($data['email'])  ){
            return Redirect::to($link_edit)
            ->withInput(Input::except('password'))
            ->with('error', 'You must fill username & email');
        }

        $user           = User::with('roles')->findOrFail($data['id']);

        $user->username = $data['username'];
        $user->email    = $data['email'];

        if(!empty($data['new_password'])){
            $user->password              = $data['new_password'];
            $user->password_confirmation = $data['new_password'];
        }

        if(!empty($data['confirmed'])){
            $user->confirmed = 1;
        }

        $msg = 'User updated successfully!!';

        if($user->save() ){

            if(!empty($data['roles'])){
                foreach($data['roles'] as $rid){

                    if(!in_array($rid, $roles_id)):

                    else:
                        $user->roles()->attach($rid);
                    endif;
                }
                $msg .= '<br>with role attached';
            }

            if(!empty($data['confirmed'])){
                $msg .= '<br>status confimed';
            }

            // Send email to user if notify checked
            if(!empty($data['notify'])){

                if(Mail::queueOn(
                    Config::get('confide::email_queue'),
                    Config::get('confide::email_account_confirmation'),
                    compact('user'),
                    function ($message) use ($user) {
                        $message
                            ->to($user->email, $user->username)
                            ->subject(Lang::get('confide::confide.email.account_confirmation.subject'));
                    }
                )){
                    $msg .= "Notification mail sent to user";
                }else{
                    $msg .= "Sent notification email failed";
                }
            }

            return Redirect::to($link_list)
            ->withInput(Input::except('password'))
            ->with('alert_success',$msg);

        }else{
            return Redirect::to($link_edit)
            ->withInput(Input::except('password'))
            ->with('error','Create user failed');
        }

    }
    public function destroy($id)
    {
        $role = User::findOrFail($id);
        $link_list = URL::to('/').'/admin/system/users';

        if($role->delete()){
            return Redirect::to($link_list)->with('alert_success','User has been deleted successfully!!');
        }else{
            return Redirect::to($link_list)->with('alert_danger','There is a problem when deleting row. Please contact our administrator if you see this message!');
        }
    }

    public function destroy_all(){

         $link_list = URL::to('/').'/admin/system/users';
         $data = Input::all();
         $a = array_keys($data);
         $b = count($data);
         $s = 0;
         for($i=0;$i<$b;$i++){
            $idx = explode('_',$a[$i]);
            if($idx[0]=="s"){
                $id = $idx[1];
                $object = User::findOrFail($id);
                $object->delete();
                $s++;
            }
         }
         if($s > 0){
            return array("status" => "success", "msg" => "All selected data have been delete successfully!!");
         }else{
            return array("status" => "error", "msg" => "Failed !! No selected data.");
         }

    }
    public function store()
    {
        $repo = App::make('UserRepository');
        $user = $repo->signup(Input::all());

        if ($user->id) {
            if (Config::get('confide::signup_email')) {
                Mail::queueOn(
                    Config::get('confide::email_queue'),
                    Config::get('confide::email_account_confirmation'),
                    compact('user'),
                    function ($message) use ($user) {
                        $message
                            ->to($user->email, $user->username)
                            ->subject(Lang::get('confide::confide.email.account_confirmation.subject'));
                    }
                );
            }

            return Redirect::action('UsersController@login')
                ->with('notice', Lang::get('confide::confide.alerts.account_created'));
        } else {
            $error = $user->errors()->all(':message');

            return Redirect::action('UsersController@create')
                ->withInput(Input::except('password'))
                ->with('error', $error);
        }
    }
    public function login()
    {
        if (Confide::user()) {
            return Redirect::to('/');
        } else {
            // return View::make(Config::get('confide::login_form'));

            $page_title = 'Home';
            $page_meta = Config::get('company.meta');

            return View::make('frontend.venedor.login', compact('page_title'));
        }
    }
    public function doLogin()
    {
        $repo = App::make('UserRepository');
        $input = Input::all();

        if ($repo->login($input)) {

            return Redirect::intended('/');

        } else {
            if ($repo->isThrottled($input)) {
                $err_msg = Lang::get('confide::confide.alerts.too_many_attempts');
            } elseif ($repo->existsButNotConfirmed($input)) {
                $err_msg = Lang::get('confide::confide.alerts.not_confirmed');
            } else {
                $err_msg = Lang::get('confide::confide.alerts.wrong_credentials');
            }



            return Redirect::action('DashboardController@login')
                ->withInput(Input::except('password'))
                ->with('error', $err_msg);
        }
    }
    public function confirm($code)
    {
        if (Confide::confirm($code)) {
            $notice_msg = Lang::get('confide::confide.alerts.confirmation');
            return Redirect::action('UsersController@login')
                ->with('notice', $notice_msg);
        } else {
            $error_msg = Lang::get('confide::confide.alerts.wrong_confirmation');
            return Redirect::action('UsersController@login')
                ->with('error', $error_msg);
        }
    }
    public function forgotPassword()
    {
        return View::make(Config::get('confide::forgot_password_form'));
    }
    public function doForgotPassword()
    {
        if (Confide::forgotPassword(Input::get('email'))) {
            $notice_msg = Lang::get('confide::confide.alerts.password_forgot');
            return Redirect::action('UsersController@login')
                ->with('notice', $notice_msg);
        } else {
            $error_msg = Lang::get('confide::confide.alerts.wrong_password_forgot');
            return Redirect::action('UsersController@doForgotPassword')
                ->withInput()
                ->with('error', $error_msg);
        }
    }
    public function resetPassword($token)
    {
        return View::make(Config::get('confide::reset_password_form'))
                ->with('token', $token);
    }
    public function doResetPassword()
    {
        $repo = App::make('UserRepository');
        $input = array(
            'token'                 =>Input::get('token'),
            'password'              =>Input::get('password'),
            'password_confirmation' =>Input::get('password_confirmation'),
        );

        // By passing an array with the token, password and confirmation
        if ($repo->resetPassword($input)) {
            $notice_msg = Lang::get('confide::confide.alerts.password_reset');
            return Redirect::action('UsersController@login')
                ->with('notice', $notice_msg);
        } else {
            $error_msg = Lang::get('confide::confide.alerts.wrong_password_reset');
            return Redirect::action('UsersController@resetPassword', array('token'=>$input['token']))
                ->withInput()
                ->with('error', $error_msg);
        }
    }
    public function logout()
    {
        Confide::logout();
        return Redirect::to(app('admin_url').'login');
    }
}
