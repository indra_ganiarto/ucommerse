<?php

class NavigationController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /navigation
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /navigation/create
	 *
	 * @return Response
	 */
	public function create()
	{
		//
		$page_title  = 'Create Menu Item';

		$vars = [
			'page_title'  => 'Create Menu Item',
			'display_name' => '',
	    'description'  => '',
	    'id'  => '',
	    'form_url'     => app('admin_url').'/system/permissions/create',
		];

		return View::make('backend.navigation_create_edit', compact('page_title','permissions'));
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /navigation
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 * GET /navigation/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /navigation/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /navigation/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /navigation/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
