<?php
use Mmenu;
use Datatable;
use DB;
class MenusController extends Controller
{
    var $params;

    function __construct(){

        $this->params['alias'] = "menu";
        $this->params['add'] = "c";
        $this->params['edit'] = "e";
        $this->params['delete'] = "d";
        $this->params['deleteall'] = "da";

        $this->params['edit_url'] = URL::to('/').'/admin/system/menus/edit';
        $this->params['delete_url'] = URL::to('/').'/admin/system/menus/delete';

    }
    public function index(){  

        $breadcrumb = array(
                            "<i class='icon-settings'></i><a href='#'>System</a><i class='fa fa-angle-right'></i>",
                            "<a href='#'>menus</a><i class='fa fa-angle-right'></i>",
                            "<a href='#'>Menu List</a></i>"
                            );
        $vars = [
            'page_title'        => 'Manage Menu',
            'name'              => 'Menu List',
            'description'       => 'Manage System Menus',
            'id'                => 'menu-list',
            'cmd'               => $this->params,
            'add_url'           => URL::to('/').'/admin/system/menus/create',
            'delete_all_url'    => URL::to('/').'/admin/system/menus/deleteall',
            'brc'               => $breadcrumb,
            'data'              => $this->menu_render_datatable(),
            'has_roles'         => array()
        ];

        return View::make('backend.menus.menu_list', $vars);

    }
    public function menu_source(){
    
        $data = \Mmenu::getMenu($this->params);
        return Datatable::query($data)
        ->showColumns('checkbox','id', 'title','slug','description','action')
        ->searchColumns('title')
        ->orderColumns('title')
        ->make();
        

    }

    public function menu_render_datatable(){

        $html = Datatable::table()
                    ->addColumn('<center><input type="checkbox" id="checkAll"></center>','Id','Title','Slug','Description','Action')
                    ->addColumnWidth('40px','100px','150px','','100px','100px')
                    ->setUrl(route('menus.datatable.list'))
                    ->render();
        return $html;
    }
    public function add(){

         $breadcrumb = array(
                            "<i class='icon-settings'></i><a href='#'>System</a><i class='fa fa-angle-right'></i>",
                            "<a href='#'>menus</a><i class='fa fa-angle-right'></i>",
                            "<a href='#'>Add New Menu</a></i>"
                            );

        $expiresAt = Carbon::now()->addMinutes(10);

        if(Cache::has('roles')){
            $roles = Cache::get('roles');
        }else{
            Cache::put('roles', Role::with('perms')->get(), $expiresAt);
            $roles = Cache::get('roles');
        }

        $vars = [
            'page_title'  => 'Menus Manage',
            'name'        => 'Add New Menu',
            'description' => 'Adding new sys menu',
            'id'          => '',
            'form_mode'   => 'add',
            'back_url'     => URL::to('/').'/admin/system/menus',
            'brc'         => $breadcrumb,
            'roles'       => $roles,
            'Menu'        => new Menu,
            'confirmed'   => 0,
            'has_roles_id'   => array(),
            'form_url'    => app('admin_url').'/system/menus/create',
        ];

        return View::make('backend.menus.menu_create_edit', $vars);
    }
    public function p_create(){
        $data = Input::all();

        $link_create = app('admin_url').'/system/menus/create';
        $link_list= app('admin_url').'/system/menus';

        if(empty($data['Menuname']) && empty($data['email'])  ){
            return Redirect::to($link_create)
            ->withInput(Input::except('password'))
            ->with('error', 'You must fill Menuname & email');
        }

        if(Menu::where('email', '=', $data['email'])->exists()){
            return Redirect::to($link_create)->with('error','Menuname/email exists try another name/email');
        }

        $Menu                    = new Menu;
        $Menu->Menuname          = $data['Menuname'];
        $Menu->email             = $data['email'];
        $Menu->password          = $data['password'];
        $Menu->password_confirmation         = $data['password'];
        $Menu->confirmation_code = md5(uniqid(mt_rand(), true));

        if(!empty($data['confirmed'])){
            $Menu->confirmed = 1;
        }

        // dd($Menu);

        if($Menu->save() ){

            $message = 'Menu created successfully!!';

            if(!empty($data['roles'])){
                foreach($data['roles'] as $rid){
                    $Menu->roles()->attach($rid);
                }
                $message += '<br>with role attached';
            }

            if(!empty($data['confirmed'])){
                $message += '<br>status confimed';
            }

            // Send email to Menu if notify checked
            if(!empty($data['notify'])){

                if(Mail::queueOn(
                    Config::get('confide::email_queue'),
                    Config::get('confide::email_account_confirmation'),
                    compact('Menu'),
                    function ($message) use ($Menu) {
                        $message
                            ->to($Menu->email, $Menu->Menuname)
                            ->subject(Lang::get('confide::confide.email.account_confirmation.subject'));
                    }
                )){
                    $message += "Notification mail sent to Menu";
                }else{
                    $message += "Sent notification email failed";
                }
            }

            return Redirect::to($link_list)->withInput(Input::except('password'))
            ->with('alert_success',$message);

        }else{
            return Redirect::to($link_create)->withInput(Input::except('password'))
            ->with('error','Create Menu failed');
        }

    }
    public function edit($id)
    {

        $menu = Menu::with('items')->findOrFail($id);
        $breadcrumb = array(
                            "<i class='icon-settings'></i><a href='#'>System</a><i class='fa fa-angle-right'></i>",
                            "<a href='#'>menus</a><i class='fa fa-angle-right'></i>",
                            "<a href='#'>Edit Menu</a></i>"
                            );
        $vars = [
            'page_title'   => 'Manage menus',
            'name'         => 'Edit Menu',
            'description'  => 'Editing Data Menu',
            'id'           => $id,
            'brc'          => $breadcrumb,
            'menu'         => $menu,
            'title'       => $menu->title,
            'slug'        => $menu->slug,
            'menu_description' => $menu->description,
            'menu_items'    => $menu->items->toHierarchy(),
            'back_url'     => URL::to('/').'/admin/system/menus',
            'update_nested_url'     => URL::to('/').'/admin/system/menus/update_hirarchy',
            'form_url'     => app('admin_url').'/system/menus/update',
            'form_mode'    => 'edit'
        ];

        return View::make('backend.menus.menu_create_edit', $vars);

    }
    public function update_nested_menu(){

         $data = Input::all();
         if(count($data)>0){

            $menu = json_decode($data['menu']);
            $mid = $data['m_id'];
            $mparentcount = count($menu);

            if($mparentcount>0){
                for($i=0;$i<$mparentcount;$i++){
                    
                    //update parent attribute
                    $id = $menu[$i]->id;
                    DB::table('menu_items')
                        ->where('id', $id)
                        ->where('m_id', $mid)
                        ->update(array('parent_id' => null,'order' => $i ));

                    if(isset($menu[$i]->children)){

                        $mchild1count = count($menu[$i]->children);
                        if($mchild1count>0){

                             //update order level 2
                             for($j=0;$j<$mchild1count;$j++){

                                $idchild1 = $menu[$i]->children[$j]->id;
                                DB::table('menu_items')
                                    ->where('id', $idchild1)
                                    ->where('m_id', $mid)
                                    ->update(array('parent_id' => $id,'order' => $j ));

                                if(isset($menu[$i]->children[$j]->children)){

                                    $mchild2count = count($menu[$i]->children[$j]->children);
                                    if($mchild2count>0){

                                         //update order level 3
                                         for($k=0;$k<$mchild2count;$k++){

                                             $idchild2 = $menu[$i]->children[$j]->children[$k]->id;
                                              DB::table('menu_items')
                                                    ->where('id', $idchild2)
                                                    ->where('m_id', $mid)
                                                    ->update(array('parent_id' => $idchild1,'order' => $k ));

                                         }
                                        
                                    }


                                }


                             }

                        }
                    }

                }
            }

            Cache::forget('sys_menu');
    
            return "menu hirarchy has changed.";
         }

    }
    public function p_update(){
        $data = Input::all();
        //return var_dump($data);
        $roles_id = Role::all()->lists('id');
        $link_edit = URL::to('/').'/admin/system/menus/edit/'.$data['id'];
        $link_list = URL::to('/').'/admin/system/menus';

        if(empty($data['Menuname']) && empty($data['email'])  ){
            return Redirect::to($link_edit)
            ->withInput(Input::except('password'))
            ->with('error', 'You must fill Menuname & email');
        }

        $Menu           = Menu::with('roles')->findOrFail($data['id']);

        $Menu->Menuname = $data['Menuname'];
        $Menu->email    = $data['email'];

        if(!empty($data['new_password'])){
            $Menu->password              = $data['new_password'];
            $Menu->password_confirmation = $data['new_password'];
        }

        if(!empty($data['confirmed'])){
            $Menu->confirmed = 1;
        }

        $msg = 'Menu updated successfully!!';

        if($Menu->save() ){

            if(!empty($data['roles'])){
                foreach($data['roles'] as $rid){

                    if(!in_array($rid, $roles_id)):

                    else:
                        $Menu->roles()->attach($rid);
                    endif;
                }
                $msg .= '<br>with role attached';
            }

            if(!empty($data['confirmed'])){
                $msg .= '<br>status confimed';
            }

            // Send email to Menu if notify checked
            if(!empty($data['notify'])){

                if(Mail::queueOn(
                    Config::get('confide::email_queue'),
                    Config::get('confide::email_account_confirmation'),
                    compact('Menu'),
                    function ($message) use ($Menu) {
                        $message
                            ->to($Menu->email, $Menu->Menuname)
                            ->subject(Lang::get('confide::confide.email.account_confirmation.subject'));
                    }
                )){
                    $msg .= "Notification mail sent to Menu";
                }else{
                    $msg .= "Sent notification email failed";
                }
            }

            return Redirect::to($link_list)
            ->withInput(Input::except('password'))
            ->with('alert_success',$msg);

        }else{
            return Redirect::to($link_edit)
            ->withInput(Input::except('password'))
            ->with('error','Create Menu failed');
        }

    }
    public function destroy($id)
    {
        $role = Menu::findOrFail($id);
        $link_list = URL::to('/').'/admin/system/menus';

        if($role->delete()){
            return Redirect::to($link_list)->with('alert_success','Menu has been deleted successfully!!');
        }else{
            return Redirect::to($link_list)->with('alert_danger','There is a problem when deleting row. Please contact our administrator if you see this message!');
        }
    }

    public function destroy_all(){

         $link_list = URL::to('/').'/admin/system/menus';
         $data = Input::all();
         $a = array_keys($data);
         $b = count($data);
         $s = 0;
         for($i=0;$i<$b;$i++){
            $idx = explode('_',$a[$i]);
            if($idx[0]=="s"){
                $id = $idx[1];
                $object = Menu::findOrFail($id);
                $object->delete();
                $s++;
            }
         }
         if($s > 0){
            return array("status" => "success", "msg" => "All selected data have been delete successfully!!");
         }else{
            return array("status" => "error", "msg" => "Failed !! No selected data.");
         }

    }
    public function store()
    {
        $repo = App::make('MenuRepository');
        $Menu = $repo->signup(Input::all());

        if ($Menu->id) {
            if (Config::get('confide::signup_email')) {
                Mail::queueOn(
                    Config::get('confide::email_queue'),
                    Config::get('confide::email_account_confirmation'),
                    compact('Menu'),
                    function ($message) use ($Menu) {
                        $message
                            ->to($Menu->email, $Menu->Menuname)
                            ->subject(Lang::get('confide::confide.email.account_confirmation.subject'));
                    }
                );
            }

            return Redirect::action('menusController@login')
                ->with('notice', Lang::get('confide::confide.alerts.account_created'));
        } else {
            $error = $Menu->errors()->all(':message');

            return Redirect::action('menusController@create')
                ->withInput(Input::except('password'))
                ->with('error', $error);
        }
    }
    public function login()
    {
        if (Confide::Menu()) {
            return Redirect::to('/');
        } else {
            // return View::make(Config::get('confide::login_form'));

            $page_title = 'Home';
            $page_meta = Config::get('company.meta');

            return View::make('frontend.venedor.login', compact('page_title'));
        }
    }
    public function doLogin()
    {
        $repo = App::make('MenuRepository');
        $input = Input::all();

        if ($repo->login($input)) {

            return Redirect::intended('/');

        } else {
            if ($repo->isThrottled($input)) {
                $err_msg = Lang::get('confide::confide.alerts.too_many_attempts');
            } elseif ($repo->existsButNotConfirmed($input)) {
                $err_msg = Lang::get('confide::confide.alerts.not_confirmed');
            } else {
                $err_msg = Lang::get('confide::confide.alerts.wrong_credentials');
            }



            return Redirect::action('DashboardController@login')
                ->withInput(Input::except('password'))
                ->with('error', $err_msg);
        }
    }
    public function confirm($code)
    {
        if (Confide::confirm($code)) {
            $notice_msg = Lang::get('confide::confide.alerts.confirmation');
            return Redirect::action('menusController@login')
                ->with('notice', $notice_msg);
        } else {
            $error_msg = Lang::get('confide::confide.alerts.wrong_confirmation');
            return Redirect::action('menusController@login')
                ->with('error', $error_msg);
        }
    }
    public function forgotPassword()
    {
        return View::make(Config::get('confide::forgot_password_form'));
    }
    public function doForgotPassword()
    {
        if (Confide::forgotPassword(Input::get('email'))) {
            $notice_msg = Lang::get('confide::confide.alerts.password_forgot');
            return Redirect::action('menusController@login')
                ->with('notice', $notice_msg);
        } else {
            $error_msg = Lang::get('confide::confide.alerts.wrong_password_forgot');
            return Redirect::action('menusController@doForgotPassword')
                ->withInput()
                ->with('error', $error_msg);
        }
    }
    public function resetPassword($token)
    {
        return View::make(Config::get('confide::reset_password_form'))
                ->with('token', $token);
    }
    public function doResetPassword()
    {
        $repo = App::make('MenuRepository');
        $input = array(
            'token'                 =>Input::get('token'),
            'password'              =>Input::get('password'),
            'password_confirmation' =>Input::get('password_confirmation'),
        );

        // By passing an array with the token, password and confirmation
        if ($repo->resetPassword($input)) {
            $notice_msg = Lang::get('confide::confide.alerts.password_reset');
            return Redirect::action('menusController@login')
                ->with('notice', $notice_msg);
        } else {
            $error_msg = Lang::get('confide::confide.alerts.wrong_password_reset');
            return Redirect::action('menusController@resetPassword', array('token'=>$input['token']))
                ->withInput()
                ->with('error', $error_msg);
        }
    }
    public function logout()
    {
        Confide::logout();
        return Redirect::to(app('admin_url').'login');
    }
}
