<?php

/* render admin menu */
function renderAdminMenu($node,$allowed=[],$active_path=[]) {

  $attr = '';
  $icon = '<span class="'.$node->icon.'"></span>';
  $url = url($node->path);
  $class = '';
  $active = '';
  $system = '';


  if( !in_array($node->access, $allowed)){
        $class .= 'hide ';
        // $class .= $node->access.'|'.' ';
  }


  $link = '<a '.$attr.' href="'.$url.'">'.$icon.'<span class="title">' . $node->title . '</span></a>';

  if( $node->isLeaf() ) {

    return '<li class="'.$class.'">' . $link . '</li>';

  } else {


    /* set dropdown if has children */
    if($node->isRoot()){
        $class .= '';

    }
    /* set dropdown if has children */

    $link = '<a '.$attr.' href="'.$url.'">'.$icon.'<span class="title">' . $node->title . '</span><span class="arrow"></span></a>';

    /* set active state */
    if($node->title=='System' && Request::is('admin/system/*')){
      $active .= ' active';
    }

    if($node->title=='Users' && Request::is('admin/system/users/*')){
      $active .= ' active';
    }

    if(($node->title=='Permissions' ) && Request::is('admin/system/permissions')){
      $active .= ' active';
    }

    // if(Request::is($node->path)){
    //   $active .= ' active';
    // }
    /* set active state */


    $html = '<li class=" '.$class.$active.' ">' .$link;

    $html .= '<ul class="sub-menu">';

    foreach($node->children as $child){
      $html .= renderAdminMenu($child,$allowed,$active_path);
    }

    $html .= '</ul>';

    $html .= '</li>';
  }

  return $html;
}
/* render admin menu */
/* render menuItems */
function renderMenuItems($node) {
  $edit = "edit_row('".$node->id."')";
  $delete = "delete_row('".$node->id."','".$node->title."')";
  $button = '
  <div class="edit btn-group btn-group-xs">
    <button type="button" class="btn btn-xs btn-primary btn-round mb-control"
    data-toggle="tooltip" data-placement="top" data-original-title="Edit" onClick="'.$edit.'">
    <i class="fa fa-pencil"></i>
    </button>
    <button type="button" class="btn btn-xs btn-danger btn-round mb-control"
    data-box="#mb-remove-row" data-toggle="tooltip" data-placement="top" data-original-title="Delete" onClick="'.$delete.'">
    <i class="fa fa-times"></i>
    </button>
  </div>
  ';

  $item = '<div class="dd-handle">' . $node->title .'</div>';
  $list = '<li class="dd-item" data-id="'.$node->id.'">'.$item.'</li>';

  if( $node->isLeaf() ) {

    return $list;

  } else {
    $html = '<li class="dd-item" data-id="'.$node->id.'">' .$item;

    $html .= '<ol class="dd-list">';

    foreach($node->children as $child)
      $html .= renderMenuItems($child);

    $html .= '</ol>';

    $html .= '</li>';
  }

  return $html;
}

 ?>
