<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

// Route::get('social', function()
// {
//   # code...
//   // start the authentication process
//   $provider = 'facebook';

//   // inital authentication route
//   SocialAuth::authenticate($provider);

//   // callback route should do this
//   $profile = SocialAuth::profile($provider, Input::get());

//   $profile->provider(); // facebook
//   $profile->info(); // the facebook profile information
// });


Route::get('/', 'FrontController@index');



// Robot for searchengine
Route::get('robots.txt', function() {

    // If on the live server, serve a nice, welcoming robots.txt.
    if (App::environment() == 'production')
    {
        Robots::addUserAgent('*');
        Robots::addSitemap('sitemap.xml');
    } else {
        // If you're on any other server, tell everyone to go away.
        Robots::addDisallow('*');
    }

    return Response::make(Robots::generate(), 200, array('Content-Type' => 'text/plain'));
});
// Robot for searchengine



// Dasboard authorization & prefixing controller
Route::group(['before'=>'auth','prefix'=>'admin'], function()
{
  Route::group(['prefix'=>'system'], function()
  {
    // clear cache menu
    Route::group(['prefix'=>'cache'], function()
    {
        Route::get('clear-all', function(){
            Cache::flush();
            return Redirect::back()->with('message','All cache cleared');
        });

        Route::get('clear-themes-registry', function(){
            Cache::forget('key');
            return Redirect::back()->with('message','All theme registry cache cleared');
        });

    });

    Route::get('help', 'DashboardController@help');
    
    Route::get('users', 'UsersController@index');
    Route::get('users/datatable/list', array('as'=>'users.datatable.list', 'uses'=>'UsersController@user_source'));
    Route::get('users/create', 'UsersController@add');
    Route::post('users/create', 'UsersController@p_create');
    Route::get('users/edit/{id}', 'UsersController@edit');
    Route::get('users/delete/{id}', 'UsersController@destroy');
    Route::post('users/deleteall', 'UsersController@destroy_all');
    Route::post('users/update', 'UsersController@p_update');

    Route::get('menus', 'MenusController@index');
    Route::get('menus/datatable/list', array('as'=>'menus.datatable.list', 'uses'=>'MenusController@menu_source'));
    Route::get('menus/create', 'MenusController@add');
    Route::post('menus/create', 'MenusController@p_create');
    Route::get('menus/edit/{id}', 'MenusController@edit');
    Route::get('menus/delete/{id}', 'MenusController@destroy');
    Route::post('menus/deleteall', 'MenusController@destroy_all');
    Route::post('menus/update', 'MenusController@p_update');
    Route::post('menus/update_hirarchy', 'MenusController@update_nested_menu');

    Route::get('roles', 'RoleController@index');
    Route::get('roles/create', 'RoleController@create');
    Route::get('roles/edit/{id}', 'RoleController@edit');
    Route::get('roles/delete/{id}', 'RoleController@destroy');

    Route::post('roles/create', 'RoleController@store');
    Route::post('roles/update', 'RoleController@update');

    Route::get('permissions', 'PermissionController@index');
    Route::get('permissions/create', 'PermissionController@create');
    Route::post('permissions/create', 'PermissionController@store');
    Route::get('permissions/edit/{id}', 'PermissionController@edit');
    Route::get('permissions/delete/{id}', 'PermissionController@destroy');

    Route::get('permissions/assign', 'PermissionController@assign');
    Route::post('permissions/bulk_assign', 'PermissionController@bulk_assign');


  });
});

Route::get('admin', 'DashboardController@index');
Route::post('admin/authorize', 'DashboardController@authorize');

// Frontend
Route::get('login', 'FrontController@login');
Route::get('logout', 'FrontController@logout');
Route::get('register', 'FrontController@register');


Route::post('authorize', 'FrontController@authorize');


// Confide routes
Route::get('users/create', 'UsersController@create');
Route::post('users', 'UsersController@store');
Route::get('users/login', 'UsersController@login');
Route::post('users/login', 'UsersController@doLogin');
Route::get('users/confirm/{code}', 'UsersController@confirm');
Route::get('users/forgot_password', 'UsersController@forgotPassword');
Route::post('users/forgot_password', 'UsersController@doForgotPassword');
Route::get('users/reset_password/{token}', 'UsersController@resetPassword');
Route::post('users/reset_password', 'UsersController@doResetPassword');
Route::get('users/logout', 'UsersController@logout');
