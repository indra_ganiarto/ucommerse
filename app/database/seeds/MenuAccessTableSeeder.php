<?php

class MenuAccessTableSeeder extends Seeder {

	public function run()
	{

    $permissions = [
        [
          'name'         => 'create_menu',
          'display_name' => 'Create Menu',
        ],
        [
          'name'         => 'edit_menu',
          'display_name' => 'Edit Menu',
        ],
        [
          'name'         => 'delete_menu',
          'display_name' => 'Delete Menu',
        ],

        // permissions access menu
        [
          'name'         => 'men_frontend',
          'display_name' => 'Menu Frontend',
        ],
        [
          'name'         => 'men_dashboard',
          'display_name' => 'Menu Dashboard',
        ],
        [
          'name'         => 'men_help',
          'display_name' => 'Menu Help',
        ],
        [
          'name'         => 'men_system',
          'display_name' => 'Menu System',
        ],
        [
          'name'         => 'men_roles',
          'display_name' => 'Menu Roles',
        ],
        [
          'name'         => 'men_add_roles',
          'display_name' => 'Menu Add Roles',
        ],
        [
          'name'         => 'men_list_roles',
          'display_name' => 'Menu List Roles',
        ],
        [
          'name'         => 'men_permissions',
          'display_name' => 'Menu Permissions',
        ],
        [
          'name'         => 'men_add_permissions',
          'display_name' => 'Menu Add Permissions',
        ],
        [
          'name'         => 'men_list_permissions',
          'display_name' => 'Menu List Permissions',
        ],

        [
          'name'         => 'men_users',
          'display_name' => 'Menu Users',
        ],
        [
          'name'         => 'men_add_users',
          'display_name' => 'Menu Add Users',
        ],
        [
          'name'         => 'men_list_users',
          'display_name' => 'Menu List Users',
        ],
        // permissions access menu




    ];

    foreach ($permissions as $perm) {
      Permission::create($perm);
    }


	}

}
