<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		$this->call('EntrustTableSeeder');
		$this->call('MenuTableSeeder');
		$this->call('MenuItemTableSeeder');
		$this->call('MenuAccessTableSeeder');
	}

}
