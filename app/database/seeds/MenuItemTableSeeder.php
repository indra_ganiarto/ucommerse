<?php

// Composer: "fzaninotto/faker": "v1.3.0"
// use Faker\Factory as Faker;

class MenuItemTableSeeder extends Seeder {

	public function run()
	{

    $menus = [

      ['id' => 1, 'title' => 'Frontend','m_id'=>1, 'order'=>0,'path'=>'/','icon'=>'desktop','access'=>'men_frontend'],
      ['id' => 2, 'title' => 'Dashboard','m_id'=>1, 'order'=>1,'path'=>'admin','icon'=>'dashboard','access'=>'men_dashboard'],
      ['id' => 3, 'title' => 'System','m_id'=>1, 'order'=>2,'icon'=>'wrench','access'=>'men_system', 'children' => [
        ['id' => 4, 'title' => 'Users','m_id'=>1, 'order'=>1,'icon'=>'users','access'=>'men_users', 'children' => [
          ['id' => 5, 'title' => 'List of Users','m_id'=>1, 'order'=>1,'icon'=>'users','access'=>'men_list_users','path'=>'admin/system/users',],
          ['id' => 6, 'title' => 'Add User','m_id'=>1, 'order'=>1,'icon'=>'user','path'=>'admin/system/users/create','access'=>'men_add_users'],
        ]],
        ['id' => 7, 'title' => 'Roles','m_id'=>1, 'order'=>1,'icon'=>'asterisk','access'=>'men_roles', 'children' => [
          ['id' => 8, 'title' => 'List of Roles','m_id'=>1, 'order'=>1,'icon'=>'asterisk','path'=>'admin/system/roles','access'=>'men_list_roles'],
          ['id' => 9, 'title' => 'Add Roles','m_id'=>1, 'order'=>1,'icon'=>'asterisk','path'=>'admin/system/roles/create','access'=>'men_add_roles']
        ]],
        ['id' => 10, 'title' => 'Permissions', 'm_id'=>1,'icon'=>'key','access'=>'men_permissions', 'children' => [
          ['id' => 11, 'title' => 'List of Permissions','m_id'=>1, 'order'=>1,'icon'=>'lock','path'=>'admin/system/permissions','access'=>'men_list_permissions'],
          ['id' => 12, 'title' => 'Add Permission','m_id'=>1, 'order'=>1,'icon'=>'unlock','path'=>'admin/system/permissions/create','access'=>'men_add_permissions']
        ]],
        // This one, as it's not present, will be deleted
        // ['id' => 8, 'title' => 'Monitors'],
      ]],
      ['id' => 13, 'title' => 'Clear Cache','m_id'=>1, 'order'=>3,'icon'=>'refresh','access'=>'men_cache', 'children' => [
        ['id' => 14, 'title' => 'All Cache','m_id'=>1, 'order'=>1,'icon'=>'refresh','path'=>'admin/system/cache/clear-all','access'=>'men_cache_clear_all'],
        ['id' => 15, 'title' => 'Theme Registry','m_id'=>1, 'order'=>1,'icon'=>'refresh','path'=>'admin/system/cache/clear-themes-registry','access'=>'men_clear_theme_registry'],
        // This one, as it's not present, will be deleted
        // ['id' => 8, 'title' => 'Monitors'],
      ]],
      ['id' => 16, 'title' => 'Help','path'=>'admin/system/help','icon'=>'question','order'=>4,'access'=>'men_help'],

    ];

    MenuItem::buildTree($menus); // => true


  }

}
