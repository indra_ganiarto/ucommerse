<?php

// Composer: "fzaninotto/faker": "v1.3.0"
// use Faker\Factory as Faker;

class MenuTableSeeder extends Seeder {

	public function run()
	{
		$menus = [
        [
          'title'         => 'System Menu',
          'slug'         => 'system_menu',
          'description' => 'Menu for system',
        ],
        [
          'title'         => 'Main Menu',
          'slug'         => 'main_menu',
          'description' => 'Default main menu',
        ],

    ];

    foreach ($menus as $menu) {
      Menu::create($menu);
    }

	}

}
