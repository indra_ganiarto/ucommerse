<?php

class EntrustTableSeeder extends Seeder {

	public function run()
	{
    // Default Roles ==============================================================================

    $sadmin = new Role;
    $sadmin->name = 'Super Admin';
    $sadmin->slug = 'super_admin';
    $sadmin->save();

    $admin = new Role;
    $admin->name = 'Admin';
    $admin->slug = 'admin';
    $admin->save();

    $customer = new Role;
    $customer->name = 'Customer';
    $customer->slug = 'customer';
    $customer->save();

    $contributor = new Role;
    $contributor->name = 'Contributor';
    $contributor->slug = 'contributor';
    $contributor->save();

    // Default Roles ==============================================================================

    // Default Permission =========================================================================
    $c_permission = new Permission;
    $c_permission->name = 'c_permission';
    $c_permission->path = 'admin/system/permissions/create';
    $c_permission->display_name = 'Create Permission';
    $c_permission->save();

    $e_permission = new Permission;
    $e_permission->name = 'e_permission';
    $e_permission->path = 'admin/system/permissions/edit/*';
    $e_permission->display_name = 'Edit Permission';
    $e_permission->save();

    $d_permission = new Permission;
    $d_permission->name = 'd_permission';
    $d_permission->path = 'admin/system/permissions/delete/*';
    $d_permission->display_name = 'Delete Permission';
    $d_permission->save();

    $l_permission = new Permission;
    $l_permission->name = 'l_permission';
    $l_permission->path = 'admin/system/permissions';
    $l_permission->display_name = 'List Permission';
    $l_permission->save();

    // Role
    $c_role = new Permission;
    $c_role->name = 'c_role';
    $c_role->path = 'admin/system/roles/create';
    $c_role->display_name = 'Create Role';
    $c_role->save();

    $e_role = new Permission;
    $e_role->name = 'e_role';
    $e_role->path = 'admin/system/roles/edit/*';
    $e_role->display_name = 'Edit Role';
    $e_role->save();

    $d_role = new Permission;
    $d_role->name = 'd_role';
    $d_role->path = 'admin/system/roles/delete/*';
    $d_role->display_name = 'Delete Role';
    $d_role->save();

    $l_role = new Permission;
    $l_role->name = 'l_role';
    $l_role->path = 'admin/system/roles';
    $l_role->display_name = 'List Role';
    $l_role->save();

    // User
    $c_user = new Permission;
    $c_user->name = 'c_user';
    $c_user->path = 'admin/system/users/create';
    $c_user->display_name = 'Create User';
    $c_user->save();

    $e_user = new Permission;
    $e_user->name = 'e_user';
    $e_user->path = 'admin/system/users/edit/*';
    $e_user->display_name = 'Edit User';
    $e_user->save();

    $d_user = new Permission;
    $d_user->name = 'd_user';
    $d_user->path = 'admin/system/users/delete/*';
    $d_user->display_name = 'Delete User';
    $d_user->save();

    $l_user = new Permission;
    $l_user->name = 'l_user';
    $l_user->path = 'admin/system/users';
    $l_user->display_name = 'List User';
    $l_user->save();

    // Page
    $c_page = new Permission;
    $c_page->name = 'c_page';
    $c_page->path = 'admin/system/pages/create';
    $c_page->display_name = 'Create Page';
    $c_page->save();

    $e_page = new Permission;
    $e_page->name = 'e_page';
    $e_page->path = 'admin/system/pages/edit/*';
    $e_page->display_name = 'Edit Page';
    $e_page->save();

    $d_page = new Permission;
    $d_page->name = 'd_page';
    $d_page->path = 'admin/system/pages/delete/*';
    $d_page->display_name = 'Delete Page';
    $d_page->save();

    $l_page = new Permission;
    $l_page->name = 'l_page';
    $l_page->path = 'admin/system/pages';
    $l_page->display_name = 'List Page';
    $l_page->save();

    // Order
    $c_order = new Permission;
    $c_order->name = 'c_order';
    $c_order->path = 'admin/system/orders/create';
    $c_order->display_name = 'Create Order';
    $c_order->save();

    $e_order = new Permission;
    $e_order->name = 'e_order';
    $e_order->path = 'admin/system/orders/edit/*';
    $e_order->display_name = 'Edit Order';
    $e_order->save();

    $d_order = new Permission;
    $d_order->name = 'd_order';
    $e_order->path = 'admin/system/orders/delete/*';
    $d_order->display_name = 'Delete Order';
    $d_order->save();

    $l_order = new Permission;
    $l_order->name = 'l_order';
    $l_order->path = 'admin/system/orders';
    $l_order->display_name = 'List Order';
    $l_order->save();


    // Order
    $c_news = new Permission;
    $c_news->name = 'c_news';
    $c_news->display_name = 'Create News';
    $c_news->save();

    $e_news = new Permission;
    $e_news->name = 'e_news';
    $e_news->display_name = 'Edit News';
    $e_news->save();

    $d_news = new Permission;
    $d_news->name = 'd_news';
    $d_news->display_name = 'Delete News';
    $d_news->save();

    $l_news = new Permission;
    $l_news->name = 'l_news';
    $l_news->path = 'admin/system/news';
    $l_news->display_name = 'List News';
    $l_news->save();

    // Default Permission =========================================================================

    // Default Users ==============================================================================
    $usr_azul                        = new User;
    $usr_azul->username              = 'azul';
    $usr_azul->email                 = 'azul@ulinulin.com';

    $usr_azul->password              = 'azul';
    $usr_azul->password_confirmation = 'azul';
    $usr_azul->confirmation_code     = md5(uniqid(mt_rand(), true));
    $usr_azul->confirmed             = 1;

    if(! $usr_azul->save()) {
      Log::info('Unable to create user '.$usr_azul->email, (array)$usr_azul->errors());
    } else {
      Log::info('Created user '.$usr_azul->email);
    }

    $usr_admin                        = new User;
    $usr_admin->username              = 'admin';
    $usr_admin->email                 = 'admin@ulinulin.com';

    $usr_admin->password              = 'admin';
    $usr_admin->password_confirmation = 'admin';
    $usr_admin->confirmation_code     = md5(uniqid(mt_rand(), true));
    $usr_admin->confirmed             = 1;

    if(! $usr_admin->save()) {
      Log::info('Unable to create user '.$usr_admin->email, (array)$usr_admin->errors());
    } else {
      Log::info('Created user '.$usr_admin->email);
    }

    $usr_customer                        = new User;
    $usr_customer->username              = 'samplecustomer';
    $usr_customer->email                 = 'sample_customer@ulinulin.com';

    $usr_customer->password              = 'samplecustomer';
    $usr_customer->password_confirmation = 'samplecustomer';
    $usr_customer->confirmation_code     = md5(uniqid(mt_rand(), true));
    $usr_customer->confirmed             = 1;

    if(! $usr_customer->save()) {
      Log::info('Unable to create user '.$usr_customer->email, (array)$usr_customer->errors());
    } else {
      Log::info('Created user '.$usr_customer->email);
    }

    // Default Users ==============================================================================


    // Assign Roles ===============================================================================

    $usr_azul->attachRole($sadmin);
    $usr_admin->attachRole($admin);
    $usr_customer->attachRole($customer);

    // Assign Roles ===============================================================================

    // Assign Permission ==========================================================================

    $sadmin->perms()->sync(
      array(
        $c_permission->id,
        $e_permission->id,
        $d_permission->id,
        $l_permission->id,
        $c_role->id,
        $e_role->id,
        $d_role->id,
        $l_role->id,
        $c_user->id,
        $e_user->id,
        $d_user->id,
        $l_user->id,
        $c_page->id,
        $e_page->id,
        $d_page->id,
        $l_page->id,
        $c_order->id,
        $e_order->id,
        $d_order->id,
        $l_order->id,
      ));

    $admin->perms()->sync(
      array(
        $c_role->id,
        $e_role->id,
        $d_role->id,
        $c_user->id,
        $e_user->id,
        $d_user->id,
        $c_page->id,
        $e_page->id,
        $d_page->id,
        $c_order->id,
        $e_order->id,
        $d_order->id,
      ));

    $customer->perms()->sync(
      array(
        $c_order->id,
        $e_order->id,
        $d_order->id,
        $l_order->id,
      ));

    $contributor->perms()->sync(
      array(
        $c_news->id,
        $e_news->id,
        $d_news->id,
        $l_news->id,
      ));

    // Assign Permission ==========================================================================


	}

}
