<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenuItemsTable extends Migration {

  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::create('menu_items', function(Blueprint $table) {
      // These columns are needed for Baum's Nested Set implementation to work.
      // Column names may be changed, but they *must* all exist and be modified
      // in the model.
      // Take a look at the model scaffold comments for details.
      // We add indexes on parent_id, lft, rgt columns by default.
      $table->bigIncrements('id');
      $table->bigInteger('parent_id')->default(NULL)->nullable()->index();
      $table->integer('lft')->nullable()->index();
      $table->integer('rgt')->nullable()->index();
      $table->integer('depth')->nullable();
      // Add needed columns here (f.ex: name, slug, path, etc.)
      // $table->string('name', 255);
      $table->integer('m_id')->nullable()->index();
      $table->string('title',48);
      $table->string('path',128)->default('#')->nullable();
      $table->string('access',64)->default('#')->nullable();
      $table->string('icon', 16)->default('square');
      $table->integer('order')->default(0);
      $table->string('caption',255)->nullable();

      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    Schema::drop('menu_items');
  }

}
