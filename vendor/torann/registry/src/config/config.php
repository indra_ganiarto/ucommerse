<?php

return array(

    /*
    |--------------------------------------------------------------------------
    | Default registry table
    |--------------------------------------------------------------------------
    |
    | Default table name. You may change it to whatever you prefer
    |
    */

    'table' => 'system_registries',
);
