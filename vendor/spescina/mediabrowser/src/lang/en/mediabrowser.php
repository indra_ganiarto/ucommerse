<?php

return array(
    'upload' => 'Upload',
    'create_folder' => 'New Folder',
    'delete_folder' => 'Delete Folder',
    'select' => 'Select',
    'cancel' => 'Cancel',
    'folder' => 'folder',
    'confirm' => 'Create'
);
