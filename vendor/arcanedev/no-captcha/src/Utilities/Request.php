<?php namespace Arcanedev\NoCaptcha\Utilities;

use Arcanedev\NoCaptcha\Contracts\Utilities\RequestInterface;
use Arcanedev\NoCaptcha\Exceptions\ApiException;
use Arcanedev\NoCaptcha\Exceptions\InvalidTypeException;
use Arcanedev\NoCaptcha\Exceptions\InvalidUrlException;

class Request implements RequestInterface
{
    /* ------------------------------------------------------------------------------------------------
     |  Properties
     | ------------------------------------------------------------------------------------------------
     */
    /**
     * URL to request
     *
     * @var string
     */
    protected $url;

    /* ------------------------------------------------------------------------------------------------
     |  Constructor
     | ------------------------------------------------------------------------------------------------
     */
    /**
     * Constructor
     */
    public function __construct()
    {
    }

    /* ------------------------------------------------------------------------------------------------
     |  Getters & Setters
     | ------------------------------------------------------------------------------------------------
     */
    /**
     * Set URL
     *
     * @param  string $url
     *
     * @return $this
     */
    protected function setUrl($url)
    {
        $this->checkUrl($url);

        $this->url = $url;

        return $this;
    }

    /* ------------------------------------------------------------------------------------------------
     |  Main functions
     | ------------------------------------------------------------------------------------------------
     */
    /**
     * Create an api request using curl
     *
     * @return string
     */
    protected function curl()
    {
        $ch     = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($ch);
        curl_close($ch);

        return $result;
    }

    /**
     * Create a simple api request using file_get_contents
     *
     * @return string
     */
    protected function simple()
    {
        $result = file_get_contents($this->url);

        return $result;
    }

    /**
     * Run the request and get response
     *
     * @param  string $url
     * @param  bool   $curled
     *
     * @return array
     */
    public function send($url, $curled = true)
    {
        $this->setUrl($url);

        $result = ($this->isCurlExists() and $curled === true)
            ? $this->curl()
            : $this->simple();

        return $this->interpretResponse($result);
    }

    /* ------------------------------------------------------------------------------------------------
     |  Check Functions
     | ------------------------------------------------------------------------------------------------
     */
    /**
     * Check URL
     *
     * @param string $url
     *
     * @throws ApiException
     * @throws InvalidTypeException
     * @throws InvalidUrlException
     */
    private function checkUrl(&$url)
    {
        if (! is_string($url)) {
            throw new InvalidTypeException(
                'The url must be a string value, '.gettype($url).' given'
            );
        }

        $url = trim($url);

        if (empty($url)) {
            throw new ApiException('The url must not be empty');
        }

        if(filter_var($url, FILTER_VALIDATE_URL) === false) {
            throw new InvalidUrlException('The url [' . $url . '] is invalid');
        }
    }

    /**
     * Check if curl exists
     *
     * @return bool
     */
    private function isCurlExists()
    {
        return function_exists('curl_version');
    }

    /**
     * Check Result
     *
     * @param string $result
     *
     * @return bool
     */
    private function checkResult($result)
    {
        return is_string($result) and ! empty($result);
    }

    /* ------------------------------------------------------------------------------------------------
     |  Other Functions
     | ------------------------------------------------------------------------------------------------
     */
    /**
     * Convert the json response to array
     *
     * @param string $result
     *
     * @return array
     */
    private function interpretResponse($result)
    {
        return $this->checkResult($result)
            ? json_decode($result, true)
            : [];
    }
}
