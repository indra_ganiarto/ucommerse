<?php namespace Modules\Page\Http\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\View;

class PageController extends Controller {

	public function index()
	{
		return View::make('page::index');
	}
	
}