<?php

Route::group(['prefix' => 'page', 'namespace' => 'Modules\Page\Http\Controllers'], function()
{
	Route::get('/', 'PageController@index');
});